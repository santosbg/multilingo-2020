import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TranslationE } from './translation.entity';
import { UserE } from './user.entity';

@Entity('rating')
export class RatingE {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    rate: number;

    @ManyToOne(type => UserE, user => user.rates)
    user: Promise<UserE>;

    @ManyToOne(type => TranslationE, translation => translation.rating)
    translation: Promise<TranslationE>;
}
