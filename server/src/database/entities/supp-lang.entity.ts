import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { UserE } from './user.entity';

@Entity('supplang')
export class SuppLangE {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({unique: true})
    name: string;

    @Column({unique: true})
    index: string;

    @Column()
    supported: boolean;

    @ManyToOne(type => UserE, users => users.editLanguages)
    public users: Promise<UserE[]>;
}
