import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ArticleE } from './article.entity';

@Entity()
export class ArticleCategoryE {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: false })
    name: string;

    @OneToMany(type => ArticleE, article => article.category)
    articles: Promise<ArticleE[]>;
}
