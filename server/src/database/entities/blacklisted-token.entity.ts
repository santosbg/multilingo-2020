import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('blacklisted-tokens')
export class BlacklistedTokenE {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({type: 'text', default: null})
    public token: string;
}
