import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { RatingE } from './rating.entity';
import { SuppLangE } from './supp-lang.entity';
import { UserE } from './user.entity';

@Entity('translation')
export class TranslationE {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    originalText: string;

    @ManyToOne(type => SuppLangE)
    originalLang: Promise<SuppLangE>;

    @Column()
    translatedText: string;

    @Column()
    type: string;

    @ManyToOne(type => SuppLangE)
    targetLang: Promise<SuppLangE>;

    @ManyToOne(type => UserE)
    translatedBy: Promise<UserE>;

    @OneToMany(type => RatingE, rating => rating.translation)
    rating: Promise<RatingE[]>;
}
