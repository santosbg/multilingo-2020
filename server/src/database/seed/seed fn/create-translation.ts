import { Connection } from 'typeorm';
import { SuppLangE } from '../../entities/supp-lang.entity';
import { TranslationE } from '../../entities/translation.entity';

export const createTranslation = async (connection: Connection, translation: { text: string, type: string }, defLanguage: SuppLangE) => {
    const translRepo = connection.getRepository(TranslationE);

    const createdTransl: TranslationE = translRepo.create({
        originalText: translation.text,
        translatedText: translation.text,
        type: translation.type,
    });
    createdTransl.originalLang = Promise.resolve(defLanguage);
    createdTransl.targetLang = Promise.resolve(defLanguage);
    await translRepo.save(createdTransl);

};
