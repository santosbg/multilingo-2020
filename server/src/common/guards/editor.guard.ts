import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { RoleTypes } from '../../features/roles/enums/role-types.enum';

@Injectable()
export class AdminGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    return user && user.roles.includes(RoleTypes.Editor);
  }
}
