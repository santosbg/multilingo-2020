import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthService } from '../../features/auth/auth.service';

@Injectable()
export class NotLoggedGuard implements CanActivate {
    public constructor(
        private readonly authService: AuthService,
    ) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token: string | undefined = request.headers.authorization;
    if (!token || await this.authService.isTokenBlacklisted({ token })) {
      throw new BadRequestException('You are not logged in!');
    }
    return true;
  }
}
