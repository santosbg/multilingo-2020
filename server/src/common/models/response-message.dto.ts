import { Expose } from 'class-transformer';

export class ResponseMessageDTO {
    @Expose()
    public message: string;
    @Expose()
    public code?: number;
}
