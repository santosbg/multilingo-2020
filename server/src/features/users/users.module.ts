import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserE } from '../../database/entities/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { RoleE } from '../../database/entities/role.entity';
import { BlacklistedTokenE } from '../../database/entities/blacklisted-token.entity';
import { BlacklistService } from './blacklist.service';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { ArticlesService } from '../articles/articles.service';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { ArticleCategoryE } from '../../database/entities/article-category.entity';
import { TranslationService } from '../translate/translation.service';
import { GoogleTranslateService } from '../translate/googleTranslate.service';
import { TranslationE } from '../../database/entities/translation.entity';
import { RatingE } from '../../database/entities/rating.entity';
import { CommentE } from '../../database/entities/comment.entity';

@Module({
    imports: [
        // tslint:disable-next-line: max-line-length
        TypeOrmModule.forFeature([UserE, RoleE, BlacklistedTokenE, SuppLangE, ArticleE, ArticleVersionE, ArticleCategoryE, TranslationE, RatingE, CommentE]),
    ],
    controllers: [UsersController],
    providers: [UsersService, BlacklistService, ArticlesService, TranslationService, GoogleTranslateService],
    exports: [UsersService, BlacklistService],
})
export class UsersModule {}
