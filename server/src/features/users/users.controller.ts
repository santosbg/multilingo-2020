import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Put, UseGuards, UseInterceptors, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { User } from '../../common/decorators/user.decorator';
import { AdminGuard } from '../../common/guards/admin.guard';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { LoggedGuard } from '../../common/guards/logged.guard';
import { NotLoggedGuard } from '../../common/guards/not-logged.guard';
import { ResponseMessageDTO } from '../../common/models/response-message.dto';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { TransformInterceptor } from '../../transformer/interceptors/transform.interceptor';
import { ShowArticleVersionDTO } from '../articles/models/show-article.dto';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { ShowCommentDTO } from '../Comments/models/show-comment.dto';
import { ChangePasswordDTO } from './models/change-password.dto';
import { ChangeRolesDTO } from './models/change-roles.dto';
import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { UpdateBanStatusDTO } from './models/update-ban-status.dto';
import { UpdateEditLanguagesDTO } from './models/update-edit-languages.dto';
import { UpdatePrefLangDTO } from './models/update-pref-lang.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { UsersService } from './users.service';

@ApiUseTags('User Controller')
@ApiBearerAuth()
@Controller('users')
export class UsersController {

    public constructor(
        private readonly usersService: UsersService,
    ) {}

    @Get()
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async getAllUsers(
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowUserDTO[]> {
        return await this.usersService.getAllUsers(loggedUser);
    }

    @Get(':id/articles')
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(new TransformInterceptor(ShowArticleVersionDTO))
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async getAllArticleByUser(
        @User() loggedUser: LoggedUserPayload,
        @Param('id') userId: string,
    ): Promise<ArticleVersionE[]> {

        return await this.usersService.getAllArticleByUser(userId, loggedUser.prefLang);
    }

    @Get(':id')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async getUserById(
        @Param('id') userId: string,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowUserDTO> {
        return await this.usersService.getUserById(loggedUser, userId);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @UseGuards(LoggedGuard)
    public async createUser(
        @Body(new ValidationPipe({whitelist: true, transform: true})) newUser: CreateUserDTO,
    ): Promise<ShowUserDTO> {
        return await this.usersService.createUser(newUser);
    }

    @Put(':id')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async updatedUser(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) update: UpdateUserDTO,
        @User() loggedUser: LoggedUserPayload,
    ) {
        return await this.usersService.updatedUser(loggedUser, userId, update);
    }

    @Put(':id/language')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async updatedUserPrefLang(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) update: UpdatePrefLangDTO,
        @User() loggedUser: LoggedUserPayload,
    ) {
        return await this.usersService.updatedUserPrefLang(loggedUser, userId, update);
    }

    @Put(':id/image')
    @HttpCode(HttpStatus.OK)
    public async updatedUserImage(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) updatedUser: UpdateUserDTO,
    ) {
        return await this.usersService.updatedUserImage(userId, updatedUser);
    }

    @Put(':id/password')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async changeUserPassword(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) changePassword: ChangePasswordDTO,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ResponseMessageDTO> {
       return await this.usersService.changeUserPassword(loggedUser, userId, changePassword);
    }

    @Put(':id/roles')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard, AdminGuard)
    public async changeRoles(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) roles: ChangeRolesDTO,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowUserDTO> {
        return this.usersService.changeRoles(loggedUser, userId, roles);
    }

    @Put(':id/edit-languages')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard, AdminGuard)
    public async updateEditLanguages(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) editLanguages: UpdateEditLanguagesDTO,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowUserDTO> {
        return this.usersService.updateEditLanguages(loggedUser, userId, editLanguages);
    }

    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard, AdminGuard)
    @Post(':id/ban-status')
    @HttpCode(HttpStatus.OK)
    public async banUser(
        @Param('id') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) ban: UpdateBanStatusDTO,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowUserDTO> {
        return await this.usersService.banUser(loggedUser, userId, ban);
    }

    @Delete(':id/ban-status')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard, AdminGuard)
    public async unBanUser(
        @Param('id') userId: string,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowUserDTO> {
        return await this.usersService.unBanUser(loggedUser, userId);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async deleteUser(
        @Param('id') userId: string,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ResponseMessageDTO> {
        const user: ShowUserDTO = await this.usersService.deleteUser(loggedUser, userId);
        return user.isDeleted ?
        {
            message: 'The user has been deleted successfully!',
            code: 200,
        }
        :
        {
            message: 'The user has been restored successfully!',
            code: 412,
        };
    }

    @Get(':id/comments')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async getUserComments(
        @Param('id') userId: string,
        @User() loggedUser: LoggedUserPayload,
    ): Promise<ShowCommentDTO[]> {
        return await this.usersService.getUserComments(userId, loggedUser.prefLang);
    }
}
