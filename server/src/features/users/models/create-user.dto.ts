import { ApiModelProperty } from '@nestjs/swagger';
import { Length, IsString } from 'class-validator';

export class CreateUserDTO {

    @ApiModelProperty({example: 'antogada'})
    @Length(5, 20, {message: 'The username should be between 5 and 20 symbols!'})
    @IsString()
    public username: string;

    @ApiModelProperty({example: '12345678'})
    @Length(5, 20, {message: 'The password should be between 5 and 20 symbols!'})
    @IsString()
    public password: string;

    @ApiModelProperty({example: 'en'})
    @IsString()
    public prefLang: string;

}
