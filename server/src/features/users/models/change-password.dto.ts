import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, IsEmail, Length, IsString } from 'class-validator';

export class ChangePasswordDTO {

    @ApiModelProperty({example: '01234567'})
    @Length(5, 20, {message: 'The new password should be between 5 and 20 symbols!'})
    @IsString()
    public newPass: string;

    @ApiModelProperty({example: '12345678'})
    @Length(5, 20, {message: 'The current password should be between 5 and 20 symbols!'})
    @IsString()
    public oldPass: string;
}
