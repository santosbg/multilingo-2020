import { ApiModelProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';

export class UpdateEditLanguagesDTO {
    @ApiModelProperty({example: ['English', 'Spanish']})
    @IsArray({message: 'Languages should be array!'})
    public editLanguages: string[];
}
