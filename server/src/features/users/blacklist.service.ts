import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { BlacklistedTokenE } from '../../database/entities/blacklisted-token.entity';
import { TokenDTO } from '../auth/models/token.dto';

@Injectable()
export class BlacklistService {

    constructor(
        @InjectRepository(BlacklistedTokenE) private readonly blacklistRepo: Repository<BlacklistedTokenE>,
    ) {}

    public async blacklistToken(token: string): Promise<TokenDTO> {

        const blacklisted: BlacklistedTokenE = await this.blacklistRepo.save({token});

        return plainToClass(TokenDTO, blacklisted, {
            excludeExtraneousValues: true,
        });
    }

    public async isBlacklisted(token: TokenDTO): Promise<boolean> {
        const blacklistRecords = await this.blacklistRepo.findOne({ select: ['token'], where: { token } });

        return blacklistRecords === undefined ? false : true;
    }

}
