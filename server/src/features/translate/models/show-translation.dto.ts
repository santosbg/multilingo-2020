import { SuppLangE } from '../../../database/entities/supp-lang.entity';
import { UserE } from '../../../database/entities/user.entity';
import { Publish } from '../../../transformer/decorators/publish';
import { PublishUserDTO } from '../../articles/models/publish-user.dto';
import { ShowLanguageDTO } from './show-language.dto';

export class ShowTranslationDTO {
    @Publish()
    id: string;

    @Publish()
    originalText: string;

    @Publish(ShowLanguageDTO)
    originalLang: SuppLangE;

    @Publish()
    translatedText: string;

    @Publish(ShowLanguageDTO)
    targetLang: SuppLangE;

    @Publish()
    type: string;

    @Publish()
    rating: number;

    @Publish(PublishUserDTO)
    translatedBy: UserE;
}
