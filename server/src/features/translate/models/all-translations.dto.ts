import { SuppLangE } from '../../../database/entities/supp-lang.entity';
import { UserE } from '../../../database/entities/user.entity';

export class AllTranslationsDTO {
    id: string;
    originalText: string;
    originalLang: Promise<SuppLangE>;
    translatedText: string;
    targetLang: Promise<SuppLangE>;
    type: string;
    rating: number;
    translatedBy: Promise<UserE>;
}
