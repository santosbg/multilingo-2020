import * as GoogleTranslationService from '@google-cloud/translate';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { GoogleTranslateOptions } from './models/google-translate-options.dto';

@Injectable()
export class GoogleTranslateService {

    private translateClient;

    constructor(
        private readonly configService: ConfigService,
    ) {
        const keyFilePath: string = this.configService.googleServiceAuth;
        this.translateClient = new GoogleTranslationService.v2.Translate({keyFile: keyFilePath});
    }

    public async detectLanguage(text: string) {
        const detection = (await this.translateClient.detect(text))[0];

        return detection;

    }

    public async translate(text: string | string[], options: GoogleTranslateOptions | string) {
        return await this.translateClient.translate(text, options);
    }
}
