import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UIComponentE } from '../../database/entities/ui-element.entity';

@Injectable()
export class UiComponentsService {
    constructor(
        @InjectRepository(UIComponentE) private readonly uiRepo: Repository<UIComponentE>,
        @InjectRepository(SuppLangE) private readonly langRepo: Repository<SuppLangE>,
        @InjectRepository(TranslationE) private readonly translRepo: Repository<TranslationE>,
    ) {}

    public async getAllUIInLang(language: string): Promise<{ [key: string]: string }> {
        const uiElements: UIComponentE[] = await this.uiRepo.find();
        const uiInLang: { [key: string]: string } = {};

        if (language === 'en') {
            uiElements.map(async (e) => {
                uiInLang[e.resourseName] = e.text;
            });

            return uiInLang;
        }

        const foundLang: SuppLangE = await this.langRepo.findOne({
            where: {
                index: language,
            },
        });

        await Promise.all(uiElements.map(async (el: UIComponentE) => {
            const foundTranslation: TranslationE = await this.translRepo.findOne({
                where: {
                    originalText: el.text,
                    targetLang: foundLang.id,
                    type: 'ui-component',
                },
                relations: ['targetLang'],
            });

            uiInLang[el.resourseName] = foundTranslation.translatedText;
        }));

        return uiInLang;
    }
}
