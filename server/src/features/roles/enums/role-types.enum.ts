export enum RoleTypes {
    Admin = 'Admin',
    Editor = 'Editor',
    Contributor = 'Contributor',
}
