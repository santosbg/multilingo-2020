import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleE } from '../../database/entities/role.entity';

@Module({
    imports: [TypeOrmModule.forFeature([RoleE])],
    controllers: [],
    providers: [],
})
export class RolesModule {}
