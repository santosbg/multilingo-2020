import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class UpdateCommentDTO {

    @ApiModelProperty({example: 'Mnogo mnogo qk tekst'})
    @IsString()
    @IsNotEmpty()
    @Length(0)
    public content: string;
}
