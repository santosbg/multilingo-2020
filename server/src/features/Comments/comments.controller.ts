import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { User } from '../../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { NotLoggedGuard } from '../../common/guards/not-logged.guard';
import { LoggedUserPayload } from './../auth/models/logged-user-payload.dto';
import { CommentsService } from './comments.service';
import { CreateCommentDTO } from './models/create-comment.dto';
import { ShowCommentDTO } from './models/show-comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';

@ApiUseTags('Comments Controller')
@Controller('articles/:articleId/comments')
export class CommentsController {

    public constructor(
        private readonly commentService: CommentsService,
    ) {}

    @Get()
    public async getAllCommentsByArticle(
        @Param('articleId') articleId: string,
        @Query('lang') language: string,
    ): Promise<ShowCommentDTO[]> {
        
        return await this.commentService.getAllCommentsByArticle(articleId, language);
    }

    @Post()
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    @HttpCode(HttpStatus.OK)
    public async createComment(
        @Param('articleId') articleId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: CreateCommentDTO,
        @User() user: LoggedUserPayload,
        ): Promise<ShowCommentDTO> {

        return await this.commentService.createComment(body, user, articleId);

    }

    @Put(':id')
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async editComment(
        @Param('id') commentId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: UpdateCommentDTO,
    ): Promise<ShowCommentDTO> {
        return this.commentService.editComment(commentId, body);
    }
}
