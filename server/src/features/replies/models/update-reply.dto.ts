import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateReplyDTO {

    @ApiModelProperty({ example: 'Mnogo qk reply'})
    public content: string;
}
