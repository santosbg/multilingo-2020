import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { CreateCommentDTO } from '../Comments/models/create-comment.dto';
import { ShowReplyDTO } from './models/show-reply.dto';
import { UpdateReplyDTO } from './models/update-reply.dto';
import { RepliesService } from './replies.service';

@ApiUseTags('Replies Controller')
@Controller('comments/:commentId/replies')
export class RepliesController {

    public constructor(
        private readonly repliesService: RepliesService,
    ) {}

    @Get()
    public async getAllRepliesByCommentId(
        @Param('commentId') commentId: string,
    ): Promise<ShowReplyDTO[]> {
        return await this.repliesService.getAllRepliesByCommentId(commentId);
    }

    @Post()
    @HttpCode(HttpStatus.OK)
    public async createReply(
        @Body() body: CreateCommentDTO,
        @Param('commentId') commentId: string,
    ): Promise<ShowReplyDTO> {

        return await this.repliesService.createReply(commentId, body);

    }

    @Delete(':id')
    public async removeReply(
        @Param('commentId') commentId: string,
        @Param('id') replyId: string,
    ) {
        return this.repliesService.removeReply(replyId, commentId);
    }

    @Put(':id')
    public async editReply(
        @Param('commentId') commentId: string,
        @Param('id') Id: string,
        @Body() body: UpdateReplyDTO,
    ): Promise<ShowReplyDTO> {
        return this.repliesService.editReply(Id, commentId, body);
    }
}
