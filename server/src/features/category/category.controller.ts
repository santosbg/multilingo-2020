import { Body, Controller, Get, Param, Post, Query, UseInterceptors } from '@nestjs/common';
import { ArticleCategoryE } from '../../database/entities/article-category.entity';
import { TransformInterceptor } from '../../transformer/interceptors/transform.interceptor';
import { CategoryService } from './category.service';
import { CreateArticleDTO } from './models/create-article.dto';
import { ShowCategoryDTO } from './models/show-category.dto';

@Controller('category')
export class CategoryController {

    constructor(
        private readonly categoryService: CategoryService,
    ) {}

    @Get()
    @UseInterceptors(new TransformInterceptor(ShowCategoryDTO))
    public async getAllCategoriesInLang(
        @Query('lang') language: string,
    ): Promise<ArticleCategoryE[]> {
        return this.categoryService.getAllCategoriesInlang(language);
    }

    @Get(':id')
    @UseInterceptors(new TransformInterceptor(ShowCategoryDTO))
    public async getCategoryById(
        @Param('id') id: string,
    ): Promise<ArticleCategoryE> {
        return this.categoryService.getCategoryById(id);
    }

    @Post()
    @UseInterceptors(new TransformInterceptor(ShowCategoryDTO))
    public async addCategories(
        @Body() body: CreateArticleDTO,
    ): Promise<ArticleCategoryE> {
        return this.categoryService.addCategories(body);
    }
}
