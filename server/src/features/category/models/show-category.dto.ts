import { Publish } from '../../../transformer/decorators/publish';

export class ShowCategoryDTO {

    @Publish()
    id: string;

    @Publish()
    name: string;
}
