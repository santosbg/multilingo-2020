import { Body, Controller, Get, Put, UseInterceptors } from '@nestjs/common';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TransformInterceptor } from '../../transformer/interceptors/transform.interceptor';
import { LanguagesService } from './languages.service';
import { LanguageDTO } from './models/language.dto';

@Controller('languages')
export class LanguagesController {

    constructor(
        private readonly laguagesService: LanguagesService,
    ) { }

    @Get()
    @UseInterceptors(new TransformInterceptor(LanguageDTO))
    getAllLanguages(): Promise<SuppLangE[]> {
        return this.laguagesService.getAllLanguages();
    }

    @Get('supported')
    @UseInterceptors(new TransformInterceptor(LanguageDTO))
    getAllSupportedLangs(): Promise<SuppLangE[]> {
        return this.laguagesService.getSupportedLanguages();
    }

    @Put('supported')
    @UseInterceptors(new TransformInterceptor(LanguageDTO))
    changeLanguagesSupportedStatus(
        @Body() names: string[],
    ): Promise<SuppLangE[]> {
        return this.laguagesService.findAndChangeSupportStatus(names);
    }

}
