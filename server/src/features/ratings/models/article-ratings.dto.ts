import { PhraseRatingDTO } from './phrase-rating.dto';

export class ArticleRatingsDTO {
    avgRating: number;
    title: PhraseRatingDTO;
    subtitle: PhraseRatingDTO;
    content: PhraseRatingDTO;
}
