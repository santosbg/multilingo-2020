export class ArticleTranslatedContentDTO {
    originalLangIndex: string;
    title: string;
    subtitle: string;
    content: string;
    category: string;
    language: string;
}
