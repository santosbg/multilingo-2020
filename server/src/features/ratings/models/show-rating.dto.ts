import { Publish } from '../../../transformer/decorators/publish';

export class ShowRatingDTO {

    @Publish()
    id: string;

    @Publish()
    rate: number;
}
