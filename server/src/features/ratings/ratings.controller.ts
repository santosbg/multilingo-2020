import { Body, Controller, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { User } from '../../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { NotLoggedGuard } from '../../common/guards/not-logged.guard';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { ArticleRatingsDTO } from './models/article-ratings.dto';
import { CreateRatingDTO } from './models/create-rating.dto';
import { RatingsService } from './ratings.service';

@Controller('')
export class RatingsController {

    constructor(
        private readonly ratingService: RatingsService,
    ) {}

    @Post('ratings/translation')
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async createRatingForTranslation(
        @Body() body: CreateRatingDTO,
        @User() user: LoggedUserPayload,
        ) {

            return this.ratingService.createRatingForTranslation(body, user);
    }

    @Get('articles/:id/ratings')
    @UseGuards(AuthGuardWithBlacklisting)
    public async getArticleTranslationRating(
        @Query('lang') lang: string,
        @Param('id') articleId: string,
        @User() user: LoggedUserPayload,
    ): Promise<ArticleRatingsDTO> {
        return this.ratingService.getRatingsByArticleContent(articleId, lang, user);
    }

    @Get('articles/:id/ratings/not-logged')
    public async getArticleTranslationRatingNotLogged(
        @Query('lang') lang: string,
        @Param('id') articleId: string,
    ): Promise<ArticleRatingsDTO> {
        return this.ratingService.getRatingsByArticleContent(articleId, lang);
    }
}
