import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { ArticleRatingsDTO } from './models/article-ratings.dto';
import { CreateRatingDTO } from './models/create-rating.dto';

@Injectable()
export class RatingsService {

    constructor(
        @InjectRepository(RatingE) private readonly ratingRepo: Repository<RatingE>,
        @InjectRepository(TranslationE) private readonly translRepo: Repository<TranslationE>,
        @InjectRepository(SuppLangE) private readonly langRepo: Repository<SuppLangE>,
        @InjectRepository(UserE) private readonly usersRepo: Repository<UserE>,
        @InjectRepository(ArticleVersionE) private readonly articlesRepo: Repository<ArticleVersionE>,

    ) {}

    public async createRatingForTranslation(body: CreateRatingDTO, user: LoggedUserPayload): Promise<RatingE> {

        // ako ne e lognat user
        if (!user) {

        }
        // const foundUser: UserE = await this.usersRepo.findOne({
        //     where: {
        //         id: '1',
        //     },
        // });

        const originalLang: SuppLangE = await this.langRepo.findOne({
            where: {
                index: body.originalLang,
            },
        });
        const target: SuppLangE = await this.langRepo.findOne({ where: { index: body.toLang } });

        const foundTransl: TranslationE = await this.translRepo.findOne({
            where: {
                translatedText: body.text,
                originalLang: originalLang.id,
                targetLang: target.id,
            },
            relations: ['targetLang', 'originalLang'],
        });

        const foundRating: RatingE = await this.ratingRepo.findOne({
            where: {
                user: user.id,
                translation: foundTransl.id,
            },
            relations: ['user', 'translation'],
        });

        if (foundRating) {
            foundRating.rate = body.rate;
            return await this.ratingRepo.save(foundRating);
        }

        const createdRating: RatingE = this.ratingRepo.create({
                rate: body.rate,
            });
        const foundUser: UserE = await this.usersRepo.findOne({
            where: {
                id: user.id,
            },
        });
        createdRating.user = Promise.resolve(foundUser);
        createdRating.translation = Promise.resolve(foundTransl);

        const saveRating: RatingE = await this.ratingRepo.save(createdRating);

        return saveRating;
    }

    public async getRatingsByArticleContent(articleId: string, toLang: string, user?: LoggedUserPayload): Promise<ArticleRatingsDTO> {

        const foundCurrArticleVersion: ArticleVersionE = await this.articlesRepo.findOne({
            where: {
                article: articleId,
                isCurrent: true,
            },
            relations: ['article'],
        });

        const article: ArticleE = await foundCurrArticleVersion.article;

        const originalLang: SuppLangE = await article.language;

        if (originalLang.index === toLang) {
            return undefined;
        }

        const targetLang: SuppLangE = await this.langRepo.findOne({
            where: {
                index: toLang,
            },
        });

        const translationContent = {
            title: foundCurrArticleVersion.title,
            subtitle: foundCurrArticleVersion.subtitle,
            content: foundCurrArticleVersion.content,
        };

        const returnRatings: ArticleRatingsDTO = {
            avgRating: undefined,
            title: undefined,
            subtitle: undefined,
            content: undefined,
        };
        let count: number = 0;
        let sum: number = 0;

        for (const key of Object.keys(translationContent)) {

            const translation = await this.translRepo.findOne({
                where: {
                    originalText: translationContent[key],
                    originalLang: originalLang.id,
                    targetLang: targetLang.id,
                },
                relations: ['originalLang', 'targetLang'],
            });

            const translRating = await this.ratingRepo
            .createQueryBuilder()
            .where('translationId = :id', { id: translation.id })
            .select('AVG(rate)', 'avg')
            .addSelect('SUM(rate)', 'sum')
            .addSelect('COUNT(rate)', 'count')
            .getRawOne();
            let myRating: RatingE;
            if (user) {
                myRating = await this.ratingRepo.findOne({
                    where: {
                        user: user.id,
                        translation: translation.id,
                    },
                    relations: ['user', 'translation'],
                });
            }
            for (const type of Object.keys(translRating)) {

                if (!translRating[type]) {
                    translRating[type] = 0;
                } else {
                    translRating[type] = +translRating[type];
                }

            }

            // Get by cookie from not logged user
            returnRatings[key] = {
                ...translRating,
                myRate: myRating ? myRating.rate : 0,
            };
            sum += translRating.avg ? +translRating.avg : 0;
            count++;
        }

        returnRatings.avgRating = sum / count;

        return returnRatings;
    }
}
