import { Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';

export class LoggedUserPayload {

    @Expose()
    @IsNotEmpty()
    public username: string;

    @Expose()
    @IsNotEmpty()
    public id: string;

    @Expose()
    public profileImg: string;

    @Expose()
    public isBanned: boolean;

    @Expose()
    public endOfBanDate: Date;

    @Expose()
    public banReason: string;

    @Expose()
    public roles: string[];

    @Expose()
    public powerPoints: number;

    @Expose()
    public prefLang: string;

    @Expose()
    public editLanguages?: string[];
}
