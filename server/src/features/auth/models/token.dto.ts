import { Expose } from 'class-transformer';
import { IsString, IsNotEmpty } from 'class-validator';

export class TokenDTO {

    @Expose()
    @IsString()
    @IsNotEmpty()
    public token: string;

}
