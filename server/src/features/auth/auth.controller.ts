import { Body, Controller, Delete, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { Token } from '../../common/decorators/token.decorator';
import { LoggedGuard } from '../../common/guards/logged.guard';
import { NotLoggedGuard } from '../../common/guards/not-logged.guard';
import { AuthService } from './auth.service';
import { LoginDTO } from './models/login.dto';

@ApiUseTags('Auth Controller')
@ApiBearerAuth()
@Controller('session')
export class AuthController {
    public constructor(private readonly authService: AuthService) {}

    @UseGuards(LoggedGuard)
    @Post()
    public async loginUser(@Body() userTologin: LoginDTO) {
      return await this.authService.login(userTologin);
    }

    @UseGuards(NotLoggedGuard)
    @Delete()
    public async logoutUser(@Token() token: string) {
      this.authService.logout(token);

      return {message: 'Successful logout!'};
    }

}
