import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, Length } from 'class-validator';

export class CreateArticleDTO {

    @ApiModelProperty({example: 'New title example'})
    @IsString()
    @Length(5, 100)
    title: string;

    @ApiModelProperty({example: 'New subtitle example'})
    @IsString()
    @Length(5, 100)
    subtitle: string;

    @ApiModelProperty({example: 'New content example'})
    @IsString()
    @Length(20, 3000)
    content: string;

    @ApiModelProperty({example: 'news'})
    @IsString()
    category: string;

    @ApiModelProperty({example: 'en'})
    @IsString()
    language: string;

    @ApiModelProperty({example: ''})
    @IsString()
    image: string;

}
