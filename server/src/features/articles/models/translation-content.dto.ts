
export class TranslationContentDTO {
    title: string;
    subtitle: string;
    content: string;
    category: string;
    langName: string;
}
