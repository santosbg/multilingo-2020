import { Publish } from '../../../transformer/decorators/publish';

export class ShowVersionDTO {
    @Publish()
    id: string;
    @Publish()
    title: string;
    @Publish()
    subtitle: string;
    @Publish()
    content: string;
    @Publish()
    versionNum: number;
    @Publish()
    isCurrent: boolean;
    @Publish()
    updatedOn: string;
    @Publish()
    image: string;

  }
