import { Module, Global } from '@nestjs/common';
import { AuthModule } from '../features/auth/auth.module';
import { ConfigModule } from '../config/config.module';

@Global()
@Module({
    imports: [AuthModule, ConfigModule],
    exports: [AuthModule, ConfigModule],
})
export class CoreModule {}
