import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../services/notification.service';
import { LoggedUserDTO } from '../../features/users/models/logged-user.dto';

@Injectable()
export class AdminGuard {

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
    ) {
  }

  public canActivate(): boolean {
    const user: LoggedUserDTO | undefined = this.authService.getUserDataIfAuthenticated();
    if (user && user.roles.includes('Admin')) {
        return true;
    }
    this.notificationService.error(`Sorry, you can't access this page!`);
    this.router.navigate(['/home']);
    return false;
  }

}
