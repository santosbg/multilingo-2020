import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseMessageDTO } from '../../common/models/response-message.dto';
import { ShowArticleVersionDTO } from '../../features/articles/models/show-article-version.dto';
import { CommentDTO } from '../../features/comments/models/comment.dto';
import { ChangePasswordDTO } from '../../features/users/models/change-password.dto';
import { ChangePrefLangDTO } from '../../features/users/models/change-pref-lang.dto';
import { ChangeRolesDTO } from '../../features/users/models/change-roles.dto';
import { ShowUserDTO } from '../../features/users/models/show-users.dto';
import { UpdateBanStatusDTO } from '../../features/users/models/update-ban-status.dto';
import { UpdateEditLanguagesDTO } from '../../features/users/models/update-edit-languages.dto';
import { UpdateUserDTO } from '../../features/users/models/update-user.dto';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    private readonly baseURL = 'http://localhost:3000';

    constructor(
        private readonly http: HttpClient,
    ) {}

    public allUsers(): Observable<ShowUserDTO[]> {
        return this.http.get<ShowUserDTO[]>(`${this.baseURL}/users`);
    }

    public getAllArticleByUser(userId: string): Observable<ShowArticleVersionDTO[]> {
        return this.http.get<ShowArticleVersionDTO[]>(`${this.baseURL}/users/${userId}/articles`);
    }

    public getUserById(id: string): Observable<ShowUserDTO> {
        return this.http.get<ShowUserDTO>(`${this.baseURL}/users/${id}`);
    }

    public updateUser(id: string, updatedUser: UpdateUserDTO): Observable<ShowUserDTO> {
        return this.http.put<ShowUserDTO>(`${this.baseURL}/users/${id}`, updatedUser);
    }

    public deleteUser(id: string): Observable<ResponseMessageDTO> {
        return this.http.delete<ResponseMessageDTO>(`${this.baseURL}/users/${id}`);
    }

    public changePassword(id: string, newPassword: ChangePasswordDTO): Observable<ResponseMessageDTO> {
        return this.http.put<ResponseMessageDTO>(`${this.baseURL}/users/${id}/password`, newPassword);
    }

    public changePrefLang(id: string, prefLang: ChangePrefLangDTO): Observable<ShowUserDTO> {
        return this.http.put<ShowUserDTO>(`${this.baseURL}/users/${id}/language`, prefLang);
    }

    public updateEditLanguages(id: string, editLangs: UpdateEditLanguagesDTO): Observable<ShowUserDTO> {
        return this.http.put<ShowUserDTO>(`${this.baseURL}/users/${id}/edit-languages`, editLangs);
    }

    public changeRoles(id: string, newRoles: ChangeRolesDTO): Observable<ShowUserDTO> {
        return this.http.put<ShowUserDTO>(`${this.baseURL}/users/${id}/roles`, newRoles);
    }

    public banUser(id: string, banStatus: UpdateBanStatusDTO): Observable<ShowUserDTO> {
        return this.http.post<ShowUserDTO>(`${this.baseURL}/users/${id}/ban-status`, banStatus);
    }

    public unBanUser(id: string): Observable<ShowUserDTO> {
        return this.http.delete<ShowUserDTO>(`${this.baseURL}/users/${id}/ban-status`);
    }

    public updateImage(id: string, updatedUser: UpdateUserDTO): Observable<ShowUserDTO> {
        return this.http.put<ShowUserDTO>(`${this.baseURL}/users/${id}/image`, updatedUser);
    }

    public getUserComments(id: string): Observable<CommentDTO[]> {
        return this.http.get<CommentDTO[]>(`${this.baseURL}/users/${id}/comments`);
    }

}

