import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ArticleRatingsDTO } from '../../features/articles/models/article-ratings.dto';
import { CreateTranslationRatingDTO } from '../../features/articles/models/create-translation-rating.dto';

@Injectable()
export class RatingsService {

  constructor(
    private readonly http: HttpClient,
  ) {}

  public getArticleTranslationRatings(articleId: string, lang: string, logged: boolean): Observable<ArticleRatingsDTO> {
    return this.http.get<ArticleRatingsDTO>(`http://localhost:3000/articles/${articleId}/ratings${logged ? '' : '/not-logged'}?lang=${lang}`);
  }

  public rateTranslation(rate: CreateTranslationRatingDTO) {
    return this.http.post<CreateTranslationRatingDTO>(`http://localhost:3000/ratings/translation`, rate);
  }


}
