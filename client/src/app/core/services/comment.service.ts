import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentDTO } from '../../features/comments/models/comment.dto';
import { DetectedLanguageDTO } from '../../features/languages/models/detected-language.dto';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

    private readonly baseURL = 'http://localhost:3000';

    constructor(
        private readonly http: HttpClient
    ) {}

    public getArticleComments(articleId: string, language: string): Observable<CommentDTO[]> {
        return this.http.get<CommentDTO[]>(`${this.baseURL}/articles/${articleId}/comments?lang=${language}`);
    }

    public editComment(articleId: string, commentId: string, newText: {content: string}): Observable<CommentDTO> {
        return this.http.put<CommentDTO>(`${this.baseURL}/articles/${articleId}/comments/${commentId}`, newText);
    }

    public createComment(articleId: string, newComment: {content: string, language: string}): Observable<CommentDTO> {
        return this.http.post<CommentDTO>(`${this.baseURL}/articles/${articleId}/comments`, newComment);
    }

    public detectCommentLang(content: { text: string }): Observable<DetectedLanguageDTO> {
        return this.http.post<DetectedLanguageDTO>(`http://localhost:3000/translations/detection`, content);
      }
}
