import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryDTO } from '../../features/admin-panel/components/admin-settings/categories/models/category.dto';
import { CreateCategoryDTO } from '../../features/admin-panel/components/admin-settings/categories/models/create-category.dto';

@Injectable()
export class CategoriesService {
  constructor(
    private readonly http: HttpClient,
  ) {}

  public getCategoryById(id: string): Observable<CategoryDTO> {
    return this.http.get<CategoryDTO>(`http://localhost:3000/category/${id}`);
  }

  public getAllCategories(lang: string): Observable<CategoryDTO[]> {
    return this.http.get<CategoryDTO[]>(`http://localhost:3000/category?lang=${lang}`);
  }

  public addCategory(category: CreateCategoryDTO): Observable<CategoryDTO> {
    return this.http.post<CategoryDTO>(`http://localhost:3000/category`, category);
  }

}
