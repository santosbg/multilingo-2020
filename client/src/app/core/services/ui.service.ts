import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class UIService {

  private readonly UISubject$: BehaviorSubject<{ [key: string]: string }> = new BehaviorSubject(null);


  constructor(
    private readonly http: HttpClient,
  ) {}

  public fireUISubjectInLang(lang: string): Observable<{msg: string}> {
    return this.http.get<{ [key: string]: string }>(`http://localhost:3000/ui-components?lang=${lang}`).pipe(
      map(
        (el) => {
          console.log(el);
          
          this.UISubject$.next(el);
          return { msg: 'Done!' };
         }
      )
    );
  }

  public get UI$() {
    return this.UISubject$.asObservable();
  }

  public getAllUIInLang(lang: string): Observable<{ [key: string]: string }> {
    return this.http.get<{ [key: string]: string }>(`http://localhost:3000/ui-components?lang=${lang}`);
  }

}
