import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ShowUserDTO } from '../../../users/models/show-users.dto';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminUsersDialogComponent } from './admin-users-dialog/admin-users-dialog.component';
import { LanguageDTO } from '../../../languages/models/language.dto';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  displayedColumns: string[] = ['username', 'createdOn', 'banStatus', 'powerPoints'];
  dataSource: MatTableDataSource<ShowUserDTO>;

  @Input()
  public users: ShowUserDTO[];
  public filteredUsers: ShowUserDTO[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input()
  public suppLanguages: LanguageDTO[];
  @Input()
  public ui: UIComponentDTO;

  constructor(
    private readonly route: ActivatedRoute,
    public dialog: MatDialog
  ) {

    const observable = this.route.data.subscribe(
      (data) => {
        this.suppLanguages = [...data.suppLanguages];
        this.users = [ ...data.users ];
        this.filteredUsers = this.users;
        const usersSource = [ ...this.filteredUsers ];
        usersSource.map((val, index) => {
          // tslint:disable-next-line: no-string-literal
          val['tableId'] = index + 1;
        });
        this.dataSource = new MatTableDataSource(usersSource);

      }
    );

    observable.unsubscribe();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public searchUser(filterValue: string) {
    this.filteredUsers = this.users;
    this.filteredUsers = this.filteredUsers.filter((user) => {
      if (user.username.toLowerCase().includes(filterValue.trim().toLowerCase())) {
        return user;
      }
    });
    this.dataSource.data = this.filteredUsers;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public openDialog(user: ShowUserDTO, languages: LanguageDTO[]) {
    const dialogRef = this.dialog.open(AdminUsersDialogComponent, {
      width: '350px',
      height: 'unset',
      data: {
        userData: user,
        languagesData: languages
      }
    });
    const sub = dialogRef.componentInstance.bannedUser.subscribe(
      (updatedUser: ShowUserDTO) => {
        this.dataSource.data = this.dataSource.data.map((currUser: ShowUserDTO) => {
          if (currUser.id === updatedUser.id) {
            currUser = updatedUser;
          }
          return currUser;
        });
      }
    );

    dialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

}
