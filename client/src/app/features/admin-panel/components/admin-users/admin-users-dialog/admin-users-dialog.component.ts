import { Component, OnInit, Inject, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material';
import { ShowUserDTO } from '../../../../users/models/show-users.dto';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../../../../../core/services/users.service';
import { NotificationService } from '../../../../../core/services/notification.service';
import {  Router } from '@angular/router';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-admin-users-dialog',
  templateUrl: './admin-users-dialog.component.html',
  styleUrls: ['./admin-users-dialog.component.css']
})
export class AdminUsersDialogComponent implements OnInit {

  public user: ShowUserDTO;

  public banForm: FormGroup;

  public editLangs = false;

  @Output()
  public bannedUser = new EventEmitter<ShowUserDTO>();

  private banDays: {value: number, viewValue: string}[] = [
    {value: 5, viewValue: '5'},
    {value: 15, viewValue: '15'},
    {value: 50, viewValue: '50'}
  ];

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  languageCtrl = new FormControl();
  filteredLanguages: Observable<string[]>;
  languages: string[] = [];
  public suppLanguages: string[];

  public editLangsForm: FormGroup;

  @ViewChild('languageInput', {static: false}) languageInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly formBuilder: FormBuilder,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
    private readonly router: Router
  ) {
    this.user = data.userData;
    this.banForm = this.formBuilder.group({
      days: ['', Validators.compose([Validators.required])],
      banReason: ['Inappropriate behavior', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(40)])],
    });

    this.suppLanguages = data.languagesData.map((lang) => lang.name);
    this.filteredLanguages = this.languageCtrl.valueChanges.pipe(
      startWith(null),
      map((language: string | null) => language ? this._filter(language) : this.suppLanguages.slice()));
  }

  ngOnInit() {
  }

  public banUser() {
    this.usersService.banUser(this.user.id, this.banForm.value).subscribe(
      (updatedUser) => {
        this.user = updatedUser;
        this.notificationService.success('The user has been succesffuly banned!');
        this.bannedUser.emit(this.user);
      },
      () => {
        this.notificationService.error('Something went wrong!Please try again later.');
      }
    );
  }

  public unBanUser() {
    this.usersService.unBanUser(this.user.id).subscribe(
      (updatedUser) => {
        this.user = updatedUser;
        this.notificationService.success('The user has been succesffuly unbanned!');
        this.bannedUser.emit(this.user);
      },
      () => {
        this.notificationService.error('Something went wrong!Please try again later.');
      }
    );
  }

  public toggleAdminRole() {
    if (this.user.roles.includes('Admin')) {
      this.user.roles = this.user.roles.filter(curRole => curRole !== 'Editor' && curRole !== 'Admin');
    } else {
      this.user.roles.push('Admin');
      if (!this.user.roles.includes('Editor')) {
        this.user.roles.push('Editor');
      }
    }
    this.changeRoles(this.user.id, this.user.roles);
  }

  public toggleEditorRole() {
    if (this.user.roles.includes('Editor')) {
      this.user.roles = this.user.roles.filter(curRole => curRole !== 'Editor');
      this.chaneEditLangs(this.user.id, []);
    } else {
      this.user.roles.push('Editor');
      this.chaneEditLangs(this.user.id, this.languages);
    }

    this.changeRoles(this.user.id, this.user.roles);
    this.editLangs = !this.editLangs;
  }

  public deleteUser() {
    this.usersService.deleteUser(this.user.id).subscribe(
      (res) => {
        this.user.isDeleted = this.user.isDeleted ? false : true;
        this.notificationService.success(res.message);
      },
      () => {
        this.notificationService.error('Something went wrong! Please try again later.');
      }
    );
  }

  private changeRoles(id: string, roles: string[]) {
    this.usersService.changeRoles(id, {roles}).subscribe(
      (updatedUser) => {
        this.user = updatedUser;
      },
      (err) => {
        this.notificationService.error(err.error.message);
      }
    );
  }

  private chaneEditLangs(id: string, langs: string[]) {
    this.usersService.updateEditLanguages(id, {editLanguages: langs}).subscribe(
      (updatedUser) => {
        this.user = updatedUser;
      },
      (err) => {
        this.notificationService.error(err.error.message);
      }
    );
  }

  public goToProfile() {
    this.router.navigate([`/users/${this.user.id}`]);
  }

  add(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.languages.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.languageCtrl.setValue(null);
    }
  }

  remove(language: string): void {
    const index = this.languages.indexOf(language);

    if (index >= 0) {
      this.languages.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.languages.push(event.option.viewValue);
    this.languageInput.nativeElement.value = '';
    this.languageCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.suppLanguages.filter(language => language.toLowerCase().indexOf(filterValue) === 0);
  }
}

