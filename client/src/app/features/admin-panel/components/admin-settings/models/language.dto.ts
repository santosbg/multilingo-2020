export class LanguageDTO {
  public id: string;
  public name: string;
  public index: string;
  public supported: boolean;
}
