export class ChangeLanguagesDTO {
  public added: string[];
  public removed: string[];
}
