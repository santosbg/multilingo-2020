import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesService } from '../../../../../../core/services/categories.service';
import { CategoryDTO } from '../models/category.dto';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {
  public category = '';
  public language = 'en';

  constructor(
    private readonly categoriesService: CategoriesService,
    private readonly router: Router
  ) {}

  ngOnInit() {}

  onSubmit() {
    this.categoriesService
      .addCategory({ name: this.category, lang: this.language })
      .subscribe(
        (savedCategory: CategoryDTO) => {
          console.log(savedCategory);
          
            this.router.navigateByUrl(
              `/articles/categories/${savedCategory.name}`
            );
        },
        error => {
          console.log(error);
        },
      );
  }
}
