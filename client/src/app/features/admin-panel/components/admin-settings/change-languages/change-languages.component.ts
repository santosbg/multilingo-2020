import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DialogService } from '../../../../../core/services/dialog.service';
import { LanguagesService } from '../../../../../core/services/languages.service';
import { UIComponentDTO } from '../../../../../models/ui-component.dto';
import { ChangeLanguagesConfirmComponent } from '../change-languages-confirm/change-languages-confirm.component';
import { ChangeLanguagesDTO } from '../models/change-languages.dto';
import { DualListFormatDTO } from '../models/dual-list-format.dto';
import { LanguageDTO } from '../models/language.dto';

@Component({
  selector: 'app-change-languages',
  templateUrl: './change-languages.component.html',
  styleUrls: ['./change-languages.component.css']
})
export class ChangeLanguagesComponent implements OnInit {

  @Input()
  public allLanguages: LanguageDTO[];
  @Input()
  public suppLanguages: LanguageDTO[];
  public confirmed: string[];
  public sourceLangs: string[];
  public format: DualListFormatDTO;
  public ui: UIComponentDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private dialogService: DialogService,
    private readonly languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.sourceLangs = this.allLanguages.map((e: LanguageDTO) => e.name);
    const supportedNames: string[] = this.suppLanguages.map(e => e.name);
    this.confirmed = this.sourceLangs.filter(e => supportedNames.includes(e));
  }

  public saveLanguages() {
    const suppInTheBeggining = this.suppLanguages.map(e => e.name);
    const removed = suppInTheBeggining.filter(e => !this.confirmed.includes(e));
    const added = this.confirmed.filter(e => !suppInTheBeggining.includes(e));

    const dialogRef = this.dialogService.open(ChangeLanguagesConfirmComponent, { added, removed });
    const changeLanguageEvent = dialogRef.componentInstance.changeLanguages.subscribe(
      (data: ChangeLanguagesDTO) => {
        this.languagesService.changeLanguagesSupportStatusByName([...data.added, ...data.removed]).subscribe(
          (updatedLanguages: LanguageDTO[]) => {
            dialogRef.close();
          }
        );

      }
    );
    dialogRef.afterClosed().subscribe(
      () => {
        changeLanguageEvent.unsubscribe();
      }
    );
  }

}
