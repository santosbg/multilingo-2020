import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChangeLanguagesDTO } from '../models/change-languages.dto';

@Component({
  selector: 'app-change-languages-confirm',
  templateUrl: './change-languages-confirm.component.html',
  styleUrls: ['./change-languages-confirm.component.css']
})
export class ChangeLanguagesConfirmComponent implements OnInit {
  public orLabel = 'or';

  public addLabel = 'add';
  public removeLabel = 'remove';

  @Output() changeLanguages: EventEmitter<ChangeLanguagesDTO> = new EventEmitter();


  constructor(
    private dialogRef: MatDialogRef<ChangeLanguagesConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ChangeLanguagesDTO,
  ) { }

  ngOnInit() {

  }

  onNoClick() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.changeLanguages.emit(this.data);
  }

}
