import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ShowUserDTO } from '../../../users/models/show-users.dto';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ArticlesService } from '../../../../core/services/articles.service';
import { ArticleVersionDTO } from '../../../articles/models/article-version.dto';
import { LoggedUserDTO } from '../../../users/models/logged-user.dto';
import { ShowArticleVersionDTO } from '../../../articles/models/show-article-version.dto';
import { AuthService } from '../../../../core/services/auth.service';
import { NotificationService } from '../../../../core/services/notification.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-admin-articles',
  templateUrl: './admin-articles.component.html',
  styleUrls: ['./admin-articles.component.css']
})
export class AdminArticlesComponent implements OnInit {
  displayedColumns: string[] = ['title', 'category', 'createdOn', 'author'];
  dataSource: MatTableDataSource<ShowUserDTO>;


  public articles: ShowArticleVersionDTO[];
  public filteredArticles;
  public loggedUser;
  @Input()
  public ui: UIComponentDTO;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly articlesService: ArticlesService,
    private readonly notificationsService: NotificationService
  ) {}

  ngOnInit() {
    this.authService.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.articlesService.getAllArticles(this.loggedUser.prefLang).subscribe(
        articles => {
          this.articles = articles;
          this.filteredArticles = this.articles;

          const articlesSource = [...this.filteredArticles];
          articlesSource.map((val, index) => {
            // tslint:disable-next-line: no-string-literal
            val['tableId'] = index + 1;
          });
          this.dataSource = new MatTableDataSource(articlesSource);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          this.notificationsService.error(err.error.message);
        }
      );
    });
  }

  public searchArticles(filterValue: string) {
    this.filteredArticles = this.articles;
    this.filteredArticles = this.filteredArticles.filter(article => {
      if (
        article.title.toLowerCase().includes(filterValue.trim().toLowerCase())
      ) {
        return article;
      }
    });
    this.dataSource.data = this.filteredArticles;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public goToArticle(articleId: string) {
    this.router.navigate([`/articles/${articleId}`]);
  }

  public deleteArticle(articleId: string) {
    // delete
  }
}
