import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LanguagesService } from '../../../core/services/languages.service';
import { LanguageDTO } from '../../languages/models/language.dto';

@Injectable({
  providedIn: 'root',
})
export class SuppLanguagesResolver implements Resolve<LanguageDTO[]> {

  constructor(
    private readonly languageService: LanguagesService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<LanguageDTO[]> {
    return this.languageService.getSuppLanguages().pipe(
      map(
        (languages) => {
          if (languages) {
            return languages;
          }
          return;
        }
      )
    );
  }
}
