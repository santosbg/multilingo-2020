import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShowUserDTO } from '../../users/models/show-users.dto';
import { TranslationsService } from './../../../core/services/translation.service';

@Component({
  selector: 'app-edit-comment',
  templateUrl: './edit-comment.component.html',
  styleUrls: ['./edit-comment.component.css']
})
export class EditCommentComponent implements OnInit {

  @Input() public commentContentToEdit: string;
  @Input() public user: ShowUserDTO;

  public editCommentForm: FormGroup;

  @Output() closedMenu = new EventEmitter<boolean>();
  @Output() newTextEvent = new EventEmitter<{content: string}>();


  constructor(
    private readonly fb: FormBuilder,
    private readonly transService: TranslationsService,
  ) {}

  ngOnInit() {
    this.editCommentForm = this.fb.group({
      content: [`${this.commentContentToEdit}`,
      Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(500)])],
    });
  }

  editComment() {
    this.transService.getAllTranslations('article').subscribe(translations => {
      translations.filter(tr => {
        if (tr.originalText === this.editCommentForm.value && tr.targetLang.index === this.user.prefLang) {
          this.transService.updateTransl(tr.id, this.editCommentForm.value);
        }
      });
    });

    this.closedMenu.emit(false);
    this.newTextEvent.emit(this.editCommentForm.value);
  }

}
