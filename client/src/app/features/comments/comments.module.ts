import { RepliesModule } from './../../../../../server/src/features/replies/replies.module';
import { CreateCommentComponent } from './create-comment/create-comment.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CommentInfoComponent } from './comment-info/comment-info.component';
import { EditCommentComponent } from './edit-comment/edit-comment.component';
import { AllCommentsComponent } from './all-comments/comments.component';

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    // RepliesModule,
  ],
  declarations: [
    AllCommentsComponent,
    CreateCommentComponent,
    CommentInfoComponent,
    EditCommentComponent,
  ],
  providers: [],
  exports: [
    AllCommentsComponent
  ]
})
export class CommentsModule {}
