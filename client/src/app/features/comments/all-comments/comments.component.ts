import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AuthService } from '../../../core/services/auth.service';
import { CommentsService } from '../../../core/services/comment.service';
import { DialogService } from '../../../core/services/dialog.service';
import { NotificationService } from '../../../core/services/notification.service';
import { LanguageConfirmDialogComponent } from '../../../shared/components/language-confirm-dialog/language-confirm-dialog.component';
import { DetectedLanguageDTO } from '../../languages/models/detected-language.dto';
import { CommentDTO } from '../models/comment.dto';
import { LanguagesService } from './../../../core/services/languages.service';
import { LanguageDTO } from './../../languages/models/language.dto';

@Component({
  selector: 'app-all-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class AllCommentsComponent implements OnInit, OnDestroy {

  @Input() public articleId: string;
  @Input() public language: string;
  public loggedUser: any;
  public comments: CommentDTO[];
  public allLangs: LanguageDTO[];


  constructor(
    private readonly commentsService: CommentsService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly dialogService: DialogService,
    private readonly langService: LanguagesService,
  ) { }

  ngOnInit() {
    this.authService.loggedUser$.subscribe(
      (loggedUser) => {
        this.loggedUser = loggedUser;
      }
    );
    this.langService.getAllLanguages().subscribe(
      (allLanguages) => {
        this.allLangs = allLanguages;
      }
    );

    this.commentsService.getArticleComments(this.articleId, this.language).subscribe(
      (res) => {
        this.comments = res.map(o => {
          o.createdOn = moment(o.createdOn).fromNow();
          return o;
        });
      },
      (err) => {
      }
    );
  }

  ngOnDestroy(): void {
  }

  editComment(editedComment: {commentId: string, newText: {content: string}}) {
    this.commentsService.editComment(this.articleId, editedComment.commentId, editedComment.newText).subscribe(
      () => {
        this.notificationService.success(`The comment was edited successfully!`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  createComment(newComment: {content: string}) {

    this.commentsService.detectCommentLang({ text: newComment.content }).subscribe(
      (detected: DetectedLanguageDTO) => {
        const confidence = +detected.confidence * 100;
        const foundLangIndex = this.allLangs.findIndex(e => e.index === detected.language);

        if (confidence === 100 && this.language === detected.language) {
          this.commentsService.createComment(this.articleId, {
            content: newComment.content,
            language: detected.language,
          }).subscribe(
              (data) => {
                this.comments = [...this.comments, data];
                this.notificationService.success(`The comment was created successfully!`);
                // this.router.navigateByUrl(`articles/${data.article.id}`);
              }
            );
        } else {
          const dataObj = { detectedLang: undefined, suppLangs: [] };
          if (confidence >= 60) {
          // We detected that that this article is in this.allLangs[foundLangIndex].name
          dataObj.detectedLang = this.allLangs[foundLangIndex];
          dataObj.suppLangs = this.allLangs.filter(e => e.supported);

          } else {
            dataObj.suppLangs = this.allLangs.filter(e => e.supported);
            dataObj.detectedLang = undefined;
          }

          const dialogRef = this.dialogService.open(
            LanguageConfirmDialogComponent,
            dataObj,
          );

          const eventSub = dialogRef.componentInstance.choosenLang.subscribe(
            (lang: LanguageDTO) => {

              this.commentsService.createComment(this.articleId, {
                content: newComment.content,
                language: lang.index,
              }).subscribe(
                (data) => {

                  this.comments = [data, ...this.comments];
                  this.notificationService.success(`The comment was created successfully!`);
                  // this.router.navigateByUrl(`articles/${data.article.id}`);
                  dialogRef.close();
                }
              );
            }
          );

          dialogRef.afterClosed().subscribe(
            () => {
              eventSub.unsubscribe();
            }
          );


      }
    }
    );
  }


}
