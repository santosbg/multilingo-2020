export class CommentDTO {

  public id: string;

  public content: string;

  public createdOn: string;

  // public isDeleted: boolean;

  public user: {
    username: string,
    roles: string[]
  };
 }
