import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AuthService } from '../../../core/services/auth.service';
import { DialogService } from '../../../core/services/dialog.service';
import { NotificationService } from '../../../core/services/notification.service';
import { RepliesService } from '../../../core/services/replies.service';
import { LanguageConfirmDialogComponent } from '../../../shared/components/language-confirm-dialog/language-confirm-dialog.component';
import { DetectedLanguageDTO } from '../../languages/models/detected-language.dto';
import { ReplyDTO } from '../models/reply.dto';
import { LanguagesService } from './../../../core/services/languages.service';
import { LanguageDTO } from './../../languages/models/language.dto';

@Component({
  selector: 'app-all-replies',
  templateUrl: './replies.component.html',
  styleUrls: ['./replies.component.css']
})
export class AllRepliesComponent implements OnInit, OnDestroy {

  @Input() public commentId: string;
  @Input() public language: string;
  public loggedUser: any;
  public replies: ReplyDTO[];
  public allLangs: LanguageDTO[];


  constructor(
    private readonly repliesService: RepliesService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly dialogService: DialogService,
    private readonly langService: LanguagesService,
  ) { }

  ngOnInit() {
    this.authService.loggedUser$.subscribe(
      (loggedUser) => {
        this.loggedUser = loggedUser;
      }
    );
    this.langService.getAllLanguages().subscribe(
      (allLanguages) => {
        this.allLangs = allLanguages;
      }
    );

    this.repliesService.getCommentsReplies(this.commentId, this.language).subscribe(
      (res) => {
        this.replies = res.map(o => {
          o.createdOn = moment(o.createdOn).fromNow();
          return o;
        });
      },
      (err) => {
      }
    );
  }

  ngOnDestroy(): void {
  }

  editReply(editedReply: {replyId: string, newText: {content: string}}) {
    this.repliesService.editReply(this.commentId, editedReply.replyId, editedReply.newText).subscribe(
      () => {
        this.notificationService.success(`The reply was edited successfully!`);
      },
      () => {
        this.notificationService.error(`Sorry a problem occurred, please try again later!`);
      }
    );
  }

  createReply(newComment: {content: string}) {

    this.repliesService.detectReplyLang({ text: newComment.content }).subscribe(
      (detected: DetectedLanguageDTO) => {
        const confidence = +detected.confidence * 100;
        const foundLangIndex = this.allLangs.findIndex(e => e.index === detected.language);

        if (confidence === 100 && this.language === detected.language) {
          this.repliesService.createReply(this.commentId, {
            content: newComment.content,
            language: detected.language,
          }).subscribe(
              (data) => {
                this.replies = [...this.replies, data];
                this.notificationService.success(`The comment was created successfully!`);
                // this.router.navigateByUrl(`articles/${data.article.id}`);
              }
            );
        } else {
          const dataObj = { detectedLang: undefined, suppLangs: [] };
          if (confidence >= 60) {
          // We detected that that this article is in this.allLangs[foundLangIndex].name
          dataObj.detectedLang = this.allLangs[foundLangIndex];
          dataObj.suppLangs = this.allLangs.filter(e => e.supported);

          } else {
            dataObj.suppLangs = this.allLangs.filter(e => e.supported);
            dataObj.detectedLang = undefined;
          }

          const dialogRef = this.dialogService.open(
            LanguageConfirmDialogComponent,
            dataObj,
          );

          const eventSub = dialogRef.componentInstance.choosenLang.subscribe(
            (lang: LanguageDTO) => {

              this.repliesService.createReply(this.commentId, {
                content: newComment.content,
                language: lang.index,
              }).subscribe(
                (data) => {

                  this.replies = [data, ...this.replies];
                  this.notificationService.success(`The comment was created successfully!`);

                  dialogRef.close();
                }
              );
            }
          );

          dialogRef.afterClosed().subscribe(
            () => {
              eventSub.unsubscribe();
            }
          );


      }
    }
    );
  }


}
