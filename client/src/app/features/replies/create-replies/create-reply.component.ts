import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-reply',
  templateUrl: './create-reply.component.html',
  styleUrls: ['./create-reply.component.css']
})
export class CreateReplyComponent implements OnInit {

  public createReplyForm: FormGroup;

  @Output() newReplyEvent = new EventEmitter<{content: string}>();

  constructor(
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.createReplyForm = this.fb.group({
      content: [``, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(500)])],
    });
  }

  createComment() {
    this.newReplyEvent.emit(this.createReplyForm.value);
    this.createReplyForm.reset();
  }

}
