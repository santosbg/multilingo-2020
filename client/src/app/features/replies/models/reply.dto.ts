export class ReplyDTO {

    public id: string;

    public content: string;

    public createdOn: string;

}
