import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { UsersRoutingModule } from './users-routes.module';
import { ProfileComponent } from './components/profile/profile.component';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { UserPreviewComponent } from './components/user-preview/user-preview.component';
import { AllUsersResolver } from './resolvers/all-users.resolver';
import { CommonModule } from '@angular/common';
import { GetLevel } from './pipes/get-level.pipe';
import { AboutComponent } from './components/about/about.component';
import { SettingsComponent } from './components/settings/settings.component';
import { UpdateImgComponent } from './components/update-img/update-img.component';
import { UserArticlesComponent } from './components/user-articles/user-articles.component';
import { UserCommentsComponent } from './components/user-comments/user-comments.component';

@NgModule({
  declarations: [
    ProfileComponent,
    AllUsersComponent,
    UserPreviewComponent,
    GetLevel,
    AboutComponent,
    SettingsComponent,
    UpdateImgComponent,
    UserArticlesComponent,
    UserCommentsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule
  ],
  providers: [
    AllUsersResolver
  ],
})
export class UsersModule { }
