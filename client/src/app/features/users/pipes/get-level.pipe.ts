import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'getLevel' })
export class GetLevel implements PipeTransform {
  transform(powerPoints: number): string {
    let level = '';
    switch (true) {
      case powerPoints === 0:
        level = 'Newbie';
        break;
      case powerPoints > 0 && powerPoints <= 10:
        level = 'Rookie';
        break;
      case powerPoints > 10 && powerPoints <= 20:
        level = 'Talented';
        break;
      case powerPoints > 20 && powerPoints <= 30:
        level = 'Skillful';
        break;
      case powerPoints > 30 && powerPoints <= 40:
        level = 'Proficient';
        break;
      case powerPoints > 40 && powerPoints <= 50:
        level = 'Experienced';
        break;
      case powerPoints > 50:
        level = 'Expert';
        break;
    }
    return level;
  }
}
