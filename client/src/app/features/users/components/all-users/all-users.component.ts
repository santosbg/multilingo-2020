import { Component, OnInit } from '@angular/core';
import { ShowUserDTO } from '../../models/show-users.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { UIService } from '../../../../core/services/ui.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {

  public users: ShowUserDTO[];
  public filteredUsers: ShowUserDTO[];

  public loggedUser: ShowUserDTO;

  public ui: UIComponentDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly uiService: UIService
  ) {
    const observable = this.route.data.subscribe(
      (data) => {
        this.users = [ ...data.users ];
        this.filteredUsers = this.users;
      },
      (err) => {
        console.log(err);
      }
    );
    observable.unsubscribe();
  }

  ngOnInit() {
    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
    );
  }

  public searchUser(filterValue: string) {
    this.filteredUsers = this.users;
    this.filteredUsers = this.filteredUsers.filter((user) => {
      if (user.username.toLowerCase().includes(filterValue.trim().toLowerCase())) {
        return user;
      }
    });
  }

  public triggerSelectUser(user: ShowUserDTO) {
    this.router.navigate([`/users/${user.id}`]);
  }

}
