import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { UIComponentDTO } from 'src/app/models/ui-component.dto';
import { ImageCropperComponent } from 'src/app/features/image-croper/componemt/image-cropper.component';
import { ImageCroppedEvent } from 'src/app/features/image-croper/interfaces';

@Component({
  selector: 'app-update-img',
  templateUrl: './update-img.component.html',
  styleUrls: ['./update-img.component.css']
})
export class UpdateImgComponent {

  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public showCropper = false;

  @Output()
  public newImage = new EventEmitter<string>();

  @Input()
  public ui: UIComponentDTO;

  @ViewChild(ImageCropperComponent,  {static: true}) imageCropper: ImageCropperComponent;

  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  save(image: string) {
    this.newImage.emit(image);
  }
  imageLoaded() {
    this.showCropper = true;
  }
  cropperReady() {
    this.save(this.croppedImage.slice(22));
  }
  loadImageFailed() {
  }

}
