import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../../core/services/auth.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { LanguagesService } from '../../../../core/services/languages.service';
import { NotificationService } from '../../../../core/services/notification.service';
import { UIService } from '../../../../core/services/ui.service';
import { UsersService } from '../../../../core/services/users.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';
import { LanguageDTO } from '../../../languages/models/language.dto';
import { LoggedUserDTO } from '../../models/logged-user.dto';
import { LoginCredentialsDTO } from '../../models/login-credential.dto';
import { ShowUserDTO } from '../../models/show-users.dto';
import { PaswordDialogComponent } from '../password-dialog/password-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public user: ShowUserDTO;

  public updateImgMenu = false;
  public image: string;

  public loggedUser: LoggedUserDTO;
  public languages: LanguageDTO[];
  public ui: UIComponentDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
    private readonly authService: AuthService,
    private readonly languagesService: LanguagesService,
    private readonly router: Router,
    private readonly notificationsService: NotificationService,
    private readonly uiService: UIService,
    private readonly dialogService: DialogService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      (data) => {
        this.user = data.user;
        this.languages = data.suppLangs;
      },
      (err) => {
        this.notificationsService.error(err.error.message);
        this.router.navigate(['home']);
      }
    );
    this.authService.loggedUser$.subscribe(
      loggedUser => {
        this.loggedUser = loggedUser;
      },
      err => {
        this.notificationsService.error(err.error.message);
        this.router.navigate(['home']);
      }
    );

    this.languagesService.displayLanguage$.subscribe(
      (lang: string) => {
      }
    );

    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
      err => {
        this.notificationsService.error(err.error.message);
        this.router.navigate(['home']);
      }
    );
  }

  public updateImage(image: string) {
    const str = image.slice(21, image.length - 1);
  }

  public toggleImageMenu() {
    this.updateImgMenu = !this.updateImgMenu;
  }

  public newImage(newImage: string) {
    this.image = newImage;
  }

  public uploadImage() {
    this.usersService
      .updateImage(this.user.id, { profileImg: this.image })
      .subscribe(
        updatedUser => {
          this.user = updatedUser;
          this.updateImgMenu = this.updateImgMenu ? false : true;
        },
        err => {
          this.notificationService.warn(err.error.message);
        }
      );
  }

  public updateAbout(aboutForm: {
    name: string;
    surname: string;
    email: string;
  }) {
    this.usersService.updateUser(this.user.id, aboutForm).subscribe(
      data => {
        this.user = data;
      },
      err => {
        this.notificationService.warn(err.error.message);
      }
    );
  }

  public changePass(changePassForm: {
    oldPass: string;
    newPass: string;
    repeatNewPass: string;
  }) {
    this.usersService.changePassword(this.user.id, changePassForm).subscribe(
      response => {
        if (response.code === 200) {
          this.notificationService.success(response.message);
        } else {
          this.notificationService.error(response.message);
        }
      },
      () => {
        this.notificationService.error(
          'Something went wrong! Please try again later.'
        );
      }
    );
  }

  public deleteUser(userId: string) {
    if (
      this.loggedUser.roles.includes('Admin') &&
      this.loggedUser.id === this.user.id
    ) {
      this.deleteProfileAsOwner(this.user.id);
    } else if (
      this.loggedUser.roles.includes('Admin') &&
      this.loggedUser.id !== this.user.id
    ) {
      this.usersService.deleteUser(userId).subscribe(
        res => {
          this.notificationService.success(res.message);
        },
        () => {
          this.notificationService.error(
            'Something went wrong! Please try again later.'
          );
        }
      );
    } else {
      this.deleteProfileAsOwner(this.user.id);
    }
  }

  private deleteProfileAsOwner(userId: string) {
    this.usersService.deleteUser(userId).subscribe(
      (res) => {
        let language = 'en';
        for (let e of navigator.languages) {
            e = e.match('-') ? e.split('-')[0] : e;
            if (this.languages.find(lang => lang.index === e)) {
              language = e;
              break;
            }
        }

        this.uiService.fireUISubjectInLang(language).subscribe(
          (_) => {
            this.languagesService.changeLanguage(language);
          }
        );
        this.authService.logout();
        this.router.navigate(['/home']);
        this.notificationService.success(res.message);
      }
    );
  }

  public changePrefLang(changeLangForm: {prefLang: string}) {
    if (this.loggedUser.roles.includes('Admin') && this.loggedUser.id !== this.user.id) {
      this.changePrefLangAsAdmin(this.user.id, changeLangForm);
    } else if (this.loggedUser.id === this.user.id) {
      const dialogRef = this.dialogService.open(PaswordDialogComponent, this.loggedUser);
      const loginEvent = dialogRef.componentInstance.loginEvent.subscribe(
        (credentials: LoginCredentialsDTO) => {
          let type = 'local';
          let token = localStorage.getItem('token');
          if (!token && sessionStorage.getItem('token')) {
            type = 'session';
            token = sessionStorage.getItem('token');
          }
          token = token ? token : sessionStorage.getItem('token') ? sessionStorage.getItem('token') : null;
          localStorage.clear(); sessionStorage.clear();

          this.authService.login(credentials).subscribe(
            (_) => {
              this.usersService.changePrefLang(this.loggedUser.id, changeLangForm).subscribe(
                (data) => {
                  localStorage.clear(); sessionStorage.clear();
                  this.authService.login(credentials).subscribe(
                    (token) => {
                      console.log(token);
                      if (this.user.prefLang !== changeLangForm.prefLang) {
                        console.log(changeLangForm.prefLang);
                        this.user.prefLang = changeLangForm.prefLang;

                        this.uiService.fireUISubjectInLang(changeLangForm.prefLang).subscribe(
                          __ => {
                            this.languagesService.changeLanguage(changeLangForm.prefLang);
                          }
                        );

                      }

                    }
                  );
                },
                (error) => {
                  if (type === 'local') {
                    localStorage.setItem('token', token);
                  } else {
                    sessionStorage.setItem('token', token);
                  }
                  dialogRef.close();
                  this.notificationService.error('There was an error updating the language');
                }
              );
            },
            (error) => {
              this.notificationService.error('Invalid login credentials');
            }
          );
        });

      }

  }

  private changePrefLangAsAdmin(userId: string, changeLangForm: {prefLang: string}) {
    this.usersService.changePrefLang(userId, changeLangForm).subscribe(
      (data) => {
        this.user = data;
        this.uiService.fireUISubjectInLang(data.prefLang).subscribe(
          _ => {
            this.languagesService.changeLanguage(data.prefLang);
          }
        );
        this.notificationService.success('Change language successful!');
      },
      err => this.notificationService.error(`${err.error.message}`)
    );
  }

  public goToArticle(articleId: string) {
    this.router.navigate([`/articles/${articleId}`]);
  }

  public checkIfOwnerOrAdmin(): boolean {
    if (!this.loggedUser) {
      return false;
    }
    return (
      this.loggedUser.id === this.user.id ||
      this.loggedUser.roles.includes('Admin')
    );
  }
}
