import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { By, HAMMER_LOADER } from '@angular/platform-browser';
import { ProfileComponent } from './profile.component';
import { SharedModule } from '../../../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from '../../users-routes.module';
import { ImageCropperModule } from '../../../image-croper/image-croper.module';
import { GetLevel } from '../../pipes/get-level.pipe';
import { AboutComponent } from '../about/about.component';
import { SettingsComponent } from '../settings/settings.component';
import { UpdateImgComponent } from '../update-img/update-img.component';
import { UserArticlesComponent } from '../user-articles/user-articles.component';
import { UserCommentsComponent } from '../user-comments/user-comments.component';
import { UsersService } from '../../../../core/services/users.service';
import { NotificationService } from '../../../../core/services/notification.service';
import { AuthService } from '../../../../core/services/auth.service';
import { LanguagesService } from '../../../../core/services/languages.service';
import { UIService } from '../../../../core/services/ui.service';
import { DialogService } from '../../../../core/services/dialog.service';
import { AllUsersComponent } from '../all-users/all-users.component';
import { UserPreviewComponent } from '../user-preview/user-preview.component';
import { ShowUserDTO } from '../../models/show-users.dto';

describe('Profile Component', () => {
  const routes: Routes = [
    // { path: 'home', redirectTo: 'home', pathMatch: 'full' },
    { path: 'users/:id', component: ProfileComponent }
  ];

  let router: Router;
  let fixture: ComponentFixture<ProfileComponent>;
  let component: ProfileComponent;

  let route;
  let usersService;
  let notificationsService;
  let uiService;
  let languagesService;
  let dialogService;
  let authService;

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    route = {
        // default initialize with empty array of heroes
        data: of({})
    };

    usersService = {
        updateImage() {},
        updateUser() {},
        changePassword() {},
        deleteUser() {},
        changePrefLang() {}
    };

    notificationsService = {
        success() {},
        warn() {},
        error() {}
    };

    uiService = {
        get UI$() {
            return of({});
        },
        fireUISubjectInLang() {}
    };

    languagesService = {
        changeLanguage() {},
        getSuppLanguages() {
            return of({});
        }
    };

    dialogService = {
        open() {}
    };

    authService = {
        logout() {},
        login() {},
        get loggedUser$() {
            return of();
        }
    };

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CommonModule,
        SharedModule,
        ImageCropperModule
      ],
      declarations: [
        ProfileComponent,
        AllUsersComponent,
        UserPreviewComponent,
        GetLevel,
        AboutComponent,
        SettingsComponent,
        UpdateImgComponent,
        UserArticlesComponent,
        UserCommentsComponent
      ],
      providers: [
        UsersService,
        NotificationService,
        AuthService,
        LanguagesService,
        NotificationService,
        UIService,
        DialogService,
        {
            provide: HAMMER_LOADER,
            useValue: () => new Promise(() => {})
        }
      ]
    })
      .overrideProvider(ActivatedRoute, { useValue: route })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(NotificationService, { useValue: notificationsService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(LanguagesService, { useValue: languagesService })
      .overrideProvider(UIService, { useValue: uiService })
      .overrideProvider(DialogService, { useValue: dialogService })
      .compileComponents();

    router = TestBed.get(Router);
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('ngOnInit ', () => {

    it('should initialize correctly with the data passed from the resolver', done => {
        // Arrange
        const user = {
           id: '1',
           username: 'Admin'
        };

        route.data = of({user});
        // Act
        component.ngOnInit();
        // Assert
        expect(component.user).toBe(user);
        done();
    });

    it('should call notificationsService.error() and router.navigate() once if the resolved does not provide data correctly', done => {
        // Arrange
        const spyNotification = jest.spyOn(notificationsService, 'error');
        const spyRouter = jest.spyOn(router, 'navigate').mockImplementation(() => Promise.resolve(true));
        route.data = throwError({error: {message: 'error'}});
        // Act
        component.ngOnInit();
        // Assert
        expect(spyNotification).toHaveBeenCalledTimes(1);
        expect(spyRouter).toHaveBeenCalledTimes(1);
        done();
    });

    it('should get the logged user', done => {
        // Arrange
        const user = {
            id: '1',
            username: 'Admin'
         };

        const spy = jest.spyOn(authService, 'loggedUser$', 'get').mockImplementation(() => of(user));
        // Act
        component.ngOnInit();
        // Assert
        expect(component.loggedUser).toBe(user);
        done();
    });

    it('should call notificationsService.error() and router.navigate() once if there is no logged user', done => {
        // Arrange
        const spy = jest.spyOn(authService, 'loggedUser$', 'get').mockImplementation(() => throwError({error: {message: 'error'}}));
        const spyNotification = jest.spyOn(notificationsService, 'error');
        const spyRouter = jest.spyOn(router, 'navigate').mockImplementation(() => Promise.resolve(true));
        // Act
        component.ngOnInit();
        // Assert
        expect(spyNotification).toHaveBeenCalledTimes(1);
        expect(spyRouter).toHaveBeenCalledTimes(1);
        done();
    });

    it('should get the supported languages', done => {
        // Arrange
        const languages = [
            {
                id: '1',
                index: 'en'
            },
            {
                id: '1',
                index: 'bg'
            }
        ];

        const spy = jest.spyOn(languagesService, 'getSuppLanguages').mockImplementation(() => of(languages));
        // Act
        component.ngOnInit();
        // Assert
        expect(component.languages).toBe(languages);
        done();
    });

    it('should call notificationsService.error() and router.navigate() once if getSuppLanguages throw Error', done => {
        // Arrange
        const spy = jest.spyOn(languagesService, 'getSuppLanguages').mockImplementation(() => throwError({error: {message: 'error'}}));
        const spyNotification = jest.spyOn(notificationsService, 'error');
        const spyRouter = jest.spyOn(router, 'navigate').mockImplementation(() => Promise.resolve(true));
        // Act
        component.ngOnInit();
        // Assert
        expect(spyNotification).toHaveBeenCalledTimes(1);
        expect(spyRouter).toHaveBeenCalledTimes(1);
        done();
    });

    it('should get the ui components', done => {
        // Arrange
        const ui = {
            delete_btn : 'Delete',
        };

        const spy = jest.spyOn(uiService, 'UI$', 'get').mockImplementation(() => of(ui));
        // Act
        component.ngOnInit();
        // Assert
        expect(component.ui).toBe(ui);
        done();
    });

    it('should call notificationsService.error() and router.navigate() once if UI$ throw Error', done => {
        // Arrange
        const spy = jest.spyOn(uiService, 'UI$', 'get').mockImplementation(() => throwError({error: {message: 'error'}}));
        const spyNotification = jest.spyOn(notificationsService, 'error');
        const spyRouter = jest.spyOn(router, 'navigate').mockImplementation(() => Promise.resolve(true));
        // Act
        component.ngOnInit();
        // Assert
        expect(spyNotification).toHaveBeenCalledTimes(1);
        expect(spyRouter).toHaveBeenCalledTimes(1);
        done();
    });

  });

  describe('uploadImage()', () => {

    it('should return a new user with the new image link', done => {
         // Arrange
         const user = new ShowUserDTO();
         user.id = '1';
         const image = '78936523ty2783ty';
         const spy = jest.spyOn(usersService, 'updateImage').mockImplementation(() => of(user));
         // Act
         component.user = user;
         component.image = image;
         component.uploadImage();
         // Assert
         expect(component.user).toBe(user);
         done();
    });

  });

});
