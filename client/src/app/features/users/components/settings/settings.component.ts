import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ShowUserDTO } from '../../models/show-users.dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../../../../core/services/notification.service';
import { LanguageDTO } from '../../../languages/models/language.dto';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  @Input()
  public user: ShowUserDTO;
  public changePassForm: FormGroup;
  public changeLangForm: FormGroup;

  @Output()
  public changePassEvent = new EventEmitter<{oldPass: string, newPass: string, repeatNewPass: string}>();
  @Output()
  public deleteUserEvent = new EventEmitter<string>();
  @Output()
  public changePrefLangEvent = new EventEmitter<{prefLang: string}>();

  public showRepeatNewPassword = false;
  public showNewPassword = false;
  public showOldPassword = false;

  @Input()
  public ui: UIComponentDTO;

  @Input()
  public languages: LanguageDTO[];


  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.changePassForm = this.formBuilder.group({
      // tslint:disable-next-line: max-line-length
      oldPass: [`${this.ui.profile_old_password}`, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(20)])],
      // tslint:disable-next-line: max-line-length
      newPass: [`${this.ui.profile_new_password}`, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(20)])],
      // tslint:disable-next-line: max-line-length
      repeatNewPass: [`${this.ui.profile_repeat_new_password}`, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(30)])],
    });

    this.changeLangForm = this.formBuilder.group({
      prefLang: [`${this.ui.profile_change_language_label}`, Validators.compose([Validators.required])],
    });
  }

  public changePassword() {
    if (this.changePassForm.controls.newPass.value === this.changePassForm.controls.repeatNewPass.value) {
      this.changePassEvent.emit(this.changePassForm.value);
      this.changePassForm.reset();
    } else {
      this.notificationService.warn('Your new password does not match!');
    }
  }

  public deleteUser(userId: string) {
    this.user.isDeleted = !this.user.isDeleted;
    this.deleteUserEvent.emit(userId);
  }

  public changePrefLang() {
    this.changePrefLangEvent.emit(this.changeLangForm.value);
    this.changeLangForm.reset();
    this.changePassForm.reset();
  }
}
