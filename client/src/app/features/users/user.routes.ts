import { Routes } from '@angular/router';
import { LoggedGuard } from '../../core/guards/logged.guard';
import { SuppLanguagesResolver } from '../admin-panel/resolvers/supp-languages.resolver';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AllUsersResolver } from './resolvers/all-users.resolver';
import { UserResolverService } from './resolvers/user.resolver';

export const userRoutes: Routes = [
    {
        path: '',
        component: AllUsersComponent,
        resolve: { users: AllUsersResolver },
        canActivate: [LoggedGuard]
    },
    {
        path: ':id',
        component: ProfileComponent,
        resolve: { user: UserResolverService, suppLangs: SuppLanguagesResolver },
        canActivate: [LoggedGuard]
    },
];
