import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangeLanguagesComponent } from './change-languages/change-languages.component';
import { AllLanguagesResolver } from './resolvers/all-languages.resolver';


const routes: Routes = [
  {
    path: '',
    component: ChangeLanguagesComponent,
    resolve: { allLanguages: AllLanguagesResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguagesRoutingModule { }
