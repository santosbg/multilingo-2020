export class ChangeLanguagesDTO {
  added: string[];
  removed: string[];
}
