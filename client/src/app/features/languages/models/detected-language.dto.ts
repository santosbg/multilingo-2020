export class DetectedLanguageDTO {
  confidence: number;
  language: string;
  input: string;
}
