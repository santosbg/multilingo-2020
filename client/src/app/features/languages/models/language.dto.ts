export class LanguageDTO {
  id: string;
  name: string;
  index: string;
  supported: boolean;
}
