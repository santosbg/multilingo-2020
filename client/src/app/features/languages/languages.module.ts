import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ChangeLanguagesComponent } from './change-languages/change-languages.component';
import { LanguagesRoutingModule } from './languages-routing.module';
import { AllLanguagesResolver } from './resolvers/all-languages.resolver';
import { SuppLanguagesResolver } from './resolvers/supp-languages.resolver';



@NgModule({
  declarations: [
    ChangeLanguagesComponent,
  ],
  imports: [
    SharedModule,
    LanguagesRoutingModule,
  ],
  providers: [
    AllLanguagesResolver,
    SuppLanguagesResolver,
  ]
})
export class LanguagesModule { }
