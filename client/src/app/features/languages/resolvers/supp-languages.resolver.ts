import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LanguagesService } from '../../../core/services/languages.service';
import { LanguageDTO } from '../models/language.dto';






@Injectable()
export class SuppLanguagesResolver implements Resolve<LanguageDTO[]> {

  constructor(
    private readonly router: Router,
    private readonly languageService: LanguagesService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<LanguageDTO[]> {
    return this.languageService.getSuppLanguages().pipe(
      map(
        (languages) => {
          if (languages) {
            return languages;
          }
          return;
        }
      )
    );
  }
}
