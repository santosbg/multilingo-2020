import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { ChangeLanguagesConfirmComponent } from '../change-languages-confirm/change-languages-confirm.component';
import { ChangeLanguagesDTO } from '../models/change-languages.dto';
import { DualListFormatDTO } from '../models/dual-list-format.dto';
import { LanguageDTO } from '../models/language.dto';

@Component({
  selector: 'app-change-languages',
  templateUrl: './change-languages.component.html',
  styleUrls: ['./change-languages.component.css']
})
export class ChangeLanguagesComponent implements OnInit {
  public allLanguages: LanguageDTO[];
  public suppLanguages: LanguageDTO[];
  public confirmed: string[];
  public sourceLangs: string[];
  public format: DualListFormatDTO;
  public ui: UIComponentDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private dialogService: DialogService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.data.subscribe(({ allLanguages }) => {
      this.allLanguages = allLanguages;
      this.suppLanguages = allLanguages.filter((e: LanguageDTO) => e.supported);
      this.sourceLangs = allLanguages.map((e: LanguageDTO) => e.name);
      const supportedNames: string[] = this.suppLanguages.map(e => e.name);
      this.confirmed = this.sourceLangs.filter(e => supportedNames.includes(e));
    });

    this.uiService.UI$.subscribe(displayUI => {
      this.ui = displayUI;
    });
  }

  public saveLanguages() {
    const suppInTheBeggining = this.suppLanguages.map(e => e.name);
    const removed = suppInTheBeggining.filter(e => !this.confirmed.includes(e));
    const added = this.confirmed.filter(e => !suppInTheBeggining.includes(e));

    const dialogRef = this.dialogService.open(ChangeLanguagesConfirmComponent, {
      changedLangs: { added, removed },
      ui: this.ui
    });
    const changeLanguageEvent = dialogRef.componentInstance.changeLanguages.subscribe(
      (data: ChangeLanguagesDTO) => {
        this.languagesService
          .changeLanguagesSupportStatusByName([...data.added, ...data.removed])
          .subscribe((updatedLanguages: LanguageDTO[]) => {
            this.router.navigateByUrl('/');
            dialogRef.close();
          });
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      changeLanguageEvent.unsubscribe();
    });
  }
}
