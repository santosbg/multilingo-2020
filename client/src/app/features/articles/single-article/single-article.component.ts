import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, zip } from 'rxjs';
import { takeLast } from 'rxjs/operators';

import { RoleTypes } from './../../../../../../server/src/features/roles/enums/role-types.enum';
import { NotificationService } from '../../../core/services/notification.service';
import { ArticlesService } from '../../../core/services/articles.service';
import { AuthService } from '../../../core/services/auth.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { LoggedUserDTO } from '../../users/models/logged-user.dto';
import { ArticleRatingsDTO } from '../models/article-ratings.dto';
import { ArticleUserDTO } from '../models/article-user.dto';
import { ArticleVersionDTO } from '../models/article-version.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';
import { RevertArticleVersionComponent } from '../revert-article-version/revert-article-version.component';
import { MESSAGES, PATHS } from '../../../common/constants';
import { CategoriesService } from '../../../core/services/categories.service';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit, OnDestroy {
  private uiSubscription: Subscription;
  private languageSubscription: Subscription;
  private userSubscription: Subscription;
  private allSubscriptions: Subscription[];

  displayLanguage: string;
  ui: UIComponentDTO;
  loggedUser: LoggedUserDTO;
  article: ShowArticleVersionDTO;
  author: ArticleUserDTO;
  ratings: ArticleRatingsDTO;
  versions: ArticleVersionDTO[];
  isArticleTranslated: boolean;
  userHasRights: boolean;
  articleToEditId: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly articlesService: ArticlesService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly categoriesService: CategoriesService,
    private readonly notificationsService: NotificationService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      ({ article }) => {
        this.article = article.article;
        this.isArticleTranslated = article.isTranslated;
        this.displayLanguage = article.language;
        this.ui = article.ui;
        this.author = this.article.article.user;
        this.articleToEditId = this.article.article.id;
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );

    this.userSubscription = this.authService.loggedUser$.subscribe(
      (loggedUser: LoggedUserDTO) => {
        this.loggedUser = loggedUser;
        this.userHasRights = this.getUserRights();
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );

    this.languageSubscription = this.languagesService.displayLanguage$.subscribe(
      (language: string) => {
        if (language !== this.displayLanguage) {
          this.uiService.UI$.subscribe(
            (ui: UIComponentDTO) => {
              this.ui = ui;
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
          this.displayLanguage = language;
          this.articlesService.getArticleByIdInLang(this.article.article.id, language).subscribe(
            (article: ShowArticleVersionDTO) => {
              this.article = article;
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
        }
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );
    this.allSubscriptions = [
      this.uiSubscription,
      this.userSubscription,
      this.languageSubscription
    ];
  }

  editArticle() {
    // const articleData = zip(
    //   this.categoriesService.getAllCategories(this.article.article.language.index),
    //   );
    //   const categories = this.categoriesService.getAllCategories(this.article.article.language.index).subscribe
    // console.log(this.article.article.id)
    // const updatedArticle = {
    //   this.
    // };
    // this.articlesService.updateArticle(this.article.article.id, { ...updateContent }).subscribe(
    //   () => {
    //     this.articlesService.getArticleByIdInLang(this.article.article.id, this.displayLanguage).subscribe(
    //       latestArticle => {
    //         this.article = latestArticle;
    //       },
    //       () => {
    //         this.notificationsService.error(MESSAGES.TRY_AGAIN);
    //       }
    //     );
    //   },
    //   () => {
    //     this.notificationsService.error(MESSAGES.TRY_AGAIN);
    //   }
    // );
  }

  // openEditDialog() {
  //   const articleData = zip(
  //     this.articlesService.getOriginalArticle(this.article.article.id),
  //     this.categoriesService.getAllCategories(this.article.article.language.index),
  //   );
  //   articleData.subscribe(
  //     ([article, categories]) => {
  //       const dialogReference = this.dialogService.open(EditArticleDialogComponent, {
  //         article,
  //         ui: this.ui,
  //         categories,
  //       });
  //       const updateEvent = dialogReference.componentInstance.update.subscribe(
  //         (updateContent: EditArticleDTO) => {
  //           this.articlesService.updateArticle(this.article.article.id, { ...updateContent }).subscribe(
  //             () => {
  //               this.articlesService.getArticleByIdInLang(this.article.article.id, this.displayLanguage).subscribe(
  //                 latestArticle => {
  //                   this.article = latestArticle;
  //                 },
  //                 () => {
  //                   this.notificationsService.error(MESSAGES.TRY_AGAIN);
  //                 }
  //               );
  //             },
  //             () => {
  //               this.notificationsService.error(MESSAGES.TRY_AGAIN);
  //             }
  //           );
  //         },
  //         () => {
  //           this.notificationsService.error(MESSAGES.TRY_AGAIN);
  //         }
  //       );
  //       dialogReference.afterClosed().subscribe(
  //         () => {
  //           updateEvent.unsubscribe();
  //         },
  //         () => {
  //           this.notificationsService.error(MESSAGES.TRY_AGAIN);
  //         }
  //       );
  //     },
  //     () => {
  //       this.notificationsService.error(MESSAGES.TRY_AGAIN);
  //     }
  //   );
  // }

  public revertArticle() {
    if (!this.versions) {
      this.articlesService.getAllArticleVersions(this.article.article.id).subscribe(
        (articleVersions: ArticleVersionDTO[]) => {
          this.versions = articleVersions;
          this.openRevertDialog();
        },
        () => {
          this.notificationsService.error(MESSAGES.TRY_AGAIN);
        }
      );
    } else {
      this.openRevertDialog();
    }
  }

  private openRevertDialog() {
    const dialogReference = this.dialogService.open(RevertArticleVersionComponent, {
      versions: this.versions.sort((currentVersion, nextVersion) => {
        return currentVersion.versionNum - nextVersion.versionNum;
      }),
      ui: this.ui
    });
    const revertArticleEvent: Subscription = dialogReference.componentInstance.revertArticle.subscribe(
      (revVersion: ArticleVersionDTO) => {
        this.articlesService.revertArticle(this.article.article.id, { versionId: revVersion.id }).subscribe(
            (reverted: ArticleVersionDTO) => {
            const currentVersionIndex = this.versions.findIndex((version: ArticleVersionDTO) => version.isCurrent);
            this.versions[currentVersionIndex].isCurrent = false;
            const newCurrentVersionIndex =
              this.versions.findIndex((version: ArticleVersionDTO) => version.id === reverted.id);
            this.versions[newCurrentVersionIndex].isCurrent = true;
            dialogReference.close();
          },
          () => {
            this.notificationsService.error(MESSAGES.TRY_AGAIN);
          }
        );
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );
    dialogReference.afterClosed().subscribe(
      () => {
      revertArticleEvent.unsubscribe();
      this.articlesService.getArticleByIdInLang(this.article.article.id, this.displayLanguage)
        .pipe(takeLast(1))
        .subscribe(
          (revertedArticleInLang: ShowArticleVersionDTO) => {
            this.article = revertedArticleInLang;
          },
          () => {
            this.notificationsService.error(MESSAGES.TRY_AGAIN);
          }
        );
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );
  }

  ngOnDestroy(): void {
    this.allSubscriptions.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

  deleteArticle() {
    this.articlesService.deleteArticle(this.article.article.id).subscribe(
      () => {
        this.router.navigateByUrl(PATHS.HOME);
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );
  }

  private getUserRights() {
    if (this.loggedUser) {
      return this.loggedUser.id === this.author.id || this.loggedUser.roles.includes(RoleTypes.Admin) ? true : false;
    } else {
      return false;
    }
  }
}
