import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ShowLanguageDTO } from './../../../../../../server/src/features/translate/models/show-language.dto';
import { ShowCategoryDTO } from './../../../../../../server/src/features/category/models/show-category.dto';
import { ArticlesService } from '../../../core/services/articles.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import {
  LanguageConfirmDialogComponent
} from '../../../shared/components/language-confirm-dialog/language-confirm-dialog.component';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { DetectedLanguageDTO } from '../../languages/models/detected-language.dto';
import { LanguageDTO } from '../../languages/models/language.dto';
import { DEFAULT_ARTICLE_IMAGE_BASE64, CONSTRAINTS, PATHS, MESSAGES } from '../../../common/constants';
import { NotificationService } from '../../../core/services/notification.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  createArticleForm: FormGroup;
  allLanguages: LanguageDTO[];
  language: string;
  ui: UIComponentDTO;
  imageBase64: string = DEFAULT_ARTICLE_IMAGE_BASE64;
  categories: CategoryDTO[];
  showDefaultImage = true;

  @ViewChild('textArea', { read: ElementRef, static: true })
  textArea: ElementRef;

  operation = 'create';
  originalArticleId: string;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly articlesService: ArticlesService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly notificationsService: NotificationService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.categories = data.categories;
      this.allLanguages = data.allLanguages;
    });

    this.languagesService.displayLanguage$.subscribe((lang: string) => {
      this.language = lang;
    });

    this.uiService.UI$.subscribe((ui: UIComponentDTO) => {
      this.ui = ui;
    });

    this.initializeForm();

    if (history.state.articleToEditId) {
      this.articlesService.getOriginalArticle(history.state.articleToEditId).subscribe(
        (article) => {
          this.operation = 'edit';
          const categoryIndex = this.categories.findIndex((currentCategory) => {
            return currentCategory.name === article.article.category.name;
          });
          this.createArticleForm.patchValue({
            title: article.title,
            subtitle: article.subtitle,
            content: article.content,
            categories: this.categories[categoryIndex].name
          });
          this.imageBase64 = article.image;
          this.originalArticleId = article.article.id;
        }
      );
    }
  }

  toggleDefaultImage() {
    this.showDefaultImage = !this.showDefaultImage;
  }

  newImage(newImageBase64) {
    this.imageBase64 = newImageBase64;
  }

  /**
   * Then takes the whole content and sends it to
   * Google to check the language if the response of
   * confidence is 100 a new article is created. If not
   * the user is asked what is the language
   */
  private publishArticle() {
    const content = this.createArticleForm.value;
    const finalImage = this.imageBase64.slice(22);
    const category: CategoryDTO = this.categories.find((currentCategory: ShowCategoryDTO) => {
      return currentCategory.name === content.category;
    });
    const wholeContent = {
      text: content.title + content.subtitle + content.content
    };
    this.articlesService.detectArticleLang(wholeContent).subscribe(
      (detected: DetectedLanguageDTO) => {
        const confidence = detected.confidence * 100;

        if (confidence === CONSTRAINTS.MAX_CONFIDENCE && this.language === detected.language) {
          const newArticle = {
            ...content,
            category: category.id,
            language: detected.language,
            image: finalImage
          };
          this.articlesService.createArticle(newArticle).subscribe(
            article => {
              this.router.navigateByUrl(`${PATHS.ARTICLES}${article.article.id}`);
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
        } else {
          const foundLangIndex = this.allLanguages.findIndex((language: ShowLanguageDTO) => {
            return language.index === detected.language;
          });
          const languageConfirmationData = { detectedLang: null, suppLangs: [] };
          if (confidence >= CONSTRAINTS.MIN_CONFIDENCE) {
            languageConfirmationData.detectedLang = this.allLanguages[foundLangIndex];
          }
          languageConfirmationData.suppLangs = this.allLanguages.filter(language => language.supported);

          const dialogRef = this.dialogService.open(
            LanguageConfirmDialogComponent,
            {
              ...languageConfirmationData,
              ui: this.ui
            }
          );

          const eventSubscription = dialogRef.componentInstance.choosenLang.subscribe(
            (lang: LanguageDTO) => {
              const newArticle = {
                ...content,
                category: category.id,
                language: lang.index,
                image: finalImage
              };
              this.articlesService.createArticle(newArticle).subscribe(
                data => {
                  this.router.navigateByUrl(`${PATHS.ARTICLES}${data.article.id}`).then(() => {
                    dialogRef.close();
                  });
                },
                () => {
                  this.notificationsService.error(MESSAGES.TRY_AGAIN);
                }
              );
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
          dialogRef.afterClosed().subscribe(
            () => {
              eventSubscription.unsubscribe();
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
        }
      }
    );
  }

  editArticle() {
    const categoryIndex = this.categories.findIndex((currentCategory) => {
      return currentCategory.name === this.createArticleForm.value.category;
    });
    const updatedArticle = {
        ...this.createArticleForm.value,
        title: this.createArticleForm.value.title,
        subtitle: this.createArticleForm.value.subtitle,
        content: this.createArticleForm.value.content,
        category: this.categories[categoryIndex].id,
        image: this.imageBase64
    };
    console.log()
    this.articlesService.updateArticle(this.originalArticleId, { ...updatedArticle }).subscribe(
      (article) => {
        this.router.navigateByUrl(`${PATHS.ARTICLES}${article.article.id}`);
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );
  }

  private initializeForm() {
    this.createArticleForm = this.formBuilder.group({
      title: [
        '',
        this.titleValidation()
      ],
      subtitle: [
        '',
        this.titleValidation()
      ],
      content: [
        '',
        this.contentValidation(),
      ],
      category: [this.categories && this.categories.length < 0 ? this.categories[0].name : [], Validators.required]
    });
  }

  private titleValidation() {
    return Validators.compose([
      Validators.required,
      Validators.minLength(CONSTRAINTS.MIN_TITLE_LENGTH),
      Validators.maxLength(CONSTRAINTS.MAX_TITLE_LENGTH)
    ]);
  }

  private contentValidation() {
    return Validators.compose([
      Validators.required,
      Validators.minLength(CONSTRAINTS.MIN_CONTENT_LENGTH),
      Validators.maxLength(CONSTRAINTS.MAX_CONTENT_LENGTH)
    ]);
  }
}
