import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArticlesService } from '../../../core/services/articles.service';
import { CategoriesService } from '../../../core/services/categories.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';

@Injectable()
export class ArticlesCategoryResolver
  implements Resolve<{ articles: ShowArticleVersionDTO[] }> {
  private isCategoryValid = false;

  constructor(
    private readonly articleService: ArticlesService,
    private readonly languagesService: LanguagesService,
    private readonly categoriesService: CategoriesService,
    private readonly uiService: UIService,
    private readonly router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<{ articles: ShowArticleVersionDTO[], ui: UIComponentDTO }> {
    let language: string;


    this.languagesService.displayLanguage$.subscribe((lang: string) => {
      language = lang;
    });


    const category = route.paramMap.get('category')
      ? route.paramMap.get('category')
      : 'news';

    this.categoriesService
      .getAllCategories('en')
      .subscribe((categories: CategoryDTO[]) => {
        this.isCategoryValid = !!categories.find(e => e.name === category);
        if (!this.isCategoryValid) {
          this.router.navigateByUrl('not-found');
          return;
        }
      });
    const data$ = zip(
        this.articleService.getArticlesFromCategoryInLang(language, category),
        this.uiService.UI$,
        this.categoriesService.getAllCategories(language),
        );


    return data$.pipe(
      map(
        ([articles, ui, categories]) => {
          return {articles, ui, language, categories };
        }),
    );

  }
}
