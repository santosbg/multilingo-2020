import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArticlesService } from '../../../core/services/articles.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';

@Injectable({
  providedIn: 'root'
})
export class LatestArticlesResolver implements Resolve<ShowArticleVersionDTO[]> {
  constructor(
    private readonly articleService: ArticlesService,
    private readonly langService: LanguagesService,
    private readonly router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ShowArticleVersionDTO[]> {
    let language: string;

    this.langService.displayLanguage$.subscribe((lang: string) => {
      language = lang;
    });

    return this.articleService.getLatestArticlesInLang(language).pipe(
      map((articles: ShowArticleVersionDTO[]) => {
        if (articles) {
          return articles;
        }
        return;
      })
    );
  }
}
