import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArticlesService } from '../../../core/services/articles.service';
import { CategoriesService } from '../../../core/services/categories.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';

@Injectable({
  providedIn: 'root'
})
export class AllArticlesResolver
  implements
    Resolve<{
      articles: ShowArticleVersionDTO[];
      categories: CategoryDTO[];
      language: string;
      ui: UIComponentDTO;
    }> {
  constructor(
    private readonly articleService: ArticlesService,
    private readonly languageService: LanguagesService,
    private readonly uiService: UIService,
    private readonly categoriesService: CategoriesService,
    private readonly router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<{
    articles: ShowArticleVersionDTO[];
    categories: CategoryDTO[];
    language: string;
    ui: UIComponentDTO;
  }> {
    let language: string;
    const subLang = this.languageService.displayLanguage$.subscribe((lang: string) => {
      language = lang;
    });

    const data$ = zip(
      this.articleService.getAllArticles(language),
      this.categoriesService.getAllCategories(language),
      this.uiService.UI$,
    );

    return data$.pipe(
      map(([articles, categories, ui]) => {
        if (articles) {
          return { articles, categories, language, ui };
        } else {
          // something
          return;
        }
      })
    );
  }
}
