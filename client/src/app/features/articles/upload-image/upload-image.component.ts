import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { UIComponentDTO } from 'src/app/models/ui-component.dto';
import { ImageCropperComponent } from '../../image-croper/componemt/image-cropper.component';
import { ImageCroppedEvent } from '../../image-croper/interfaces';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent {
  public imageChangedEvent = '';
  public croppedImage = '';
  public showCropper = false;

  @Output()
  toggleDefaultImageEvent: EventEmitter<void> = new EventEmitter();
  @Output()
  public newImageEvent = new EventEmitter<string>();
  @Input()
  public ui: UIComponentDTO;

  @ViewChild(ImageCropperComponent, { static: true })
  imageCropper: ImageCropperComponent;

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  toggleDefaultImage() {
    this.toggleDefaultImageEvent.emit(null);
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  crop(image: string) {
    this.newImageEvent.emit(image);
  }

  imageLoaded() {
    this.toggleDefaultImage();
    this.showCropper = true;
  }

  cancelNewImage() {
    this.toggleDefaultImage();
    this.showCropper = false;
  }

  cropReady() {
    this.showCropper = false;
    this.toggleDefaultImage();
    this.newImageEvent.emit(this.croppedImage);
  }

  cropperReady() {
    this.crop(this.croppedImage);
  }
}
