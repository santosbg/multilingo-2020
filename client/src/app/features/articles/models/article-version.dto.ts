export class ArticleVersionDTO {
  id: string;
  title: string;
  subtitle: string;
  content: string;
  versionNum: number;
  isCurrent: boolean;
  updatedOn: string;
}
