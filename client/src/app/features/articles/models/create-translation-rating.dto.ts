export class CreateTranslationRatingDTO {
  text: string;
  originalLang: string;
  toLang: string;
  rate: number;
}
