export class CreateArticleDTO {
  title: string;
  subtitle: string;
  content: string;
  category: string;
  language: string;
  image: string;
}
