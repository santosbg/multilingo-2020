import { LanguageDTO } from '../../languages/models/language.dto';
import { ArticleCategoryDTO } from './article-category.dto';
import { ArticleUserDTO } from './article-user.dto';

export class ArticleInfoDTO {
  id: string;
  createdOn: string;
  category: ArticleCategoryDTO;
  user: ArticleUserDTO;
  language: LanguageDTO;
}
