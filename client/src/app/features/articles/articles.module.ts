import { NgModule } from '@angular/core';

import {
  CategoriesNavigationComponent
} from '../../shared/components/categories-navigation/categories-navigation.component';
import { SharedModule } from '../../shared/shared.module';
import { AllLanguagesResolver } from '../languages/resolvers/all-languages.resolver';
import { CommentsModule } from './../comments/comments.module';
import { AllArticlesComponent } from './all-articles/all-articles.component';
import { ArticleRatingsComponent } from './article-ratings/article-ratings.component';
import { ArticlesRoutingModule } from './articles-routing.module';
import { CreateArticleComponent } from './create-article/create-article.component';
import { AllArticlesResolver } from './resolvers/all-articles.resolver';
import { AllCategoriesResolver } from './resolvers/all-categories.resolver';
import { ArticlesCategoryResolver } from './resolvers/articles-category.resolver';
import { SingleArticleResolver } from './resolvers/single-article.resolver';
import { SingleArticleComponent } from './single-article/single-article.component';

@NgModule({
  declarations: [
    CreateArticleComponent,
    AllArticlesComponent,
    SingleArticleComponent,
    ArticleRatingsComponent,
    CategoriesNavigationComponent
  ],
  imports: [
    SharedModule,
    ArticlesRoutingModule,
    CommentsModule
  ],
  providers: [
    SingleArticleResolver,
    ArticlesCategoryResolver,
    AllArticlesResolver,
    AllCategoriesResolver,
    AllLanguagesResolver
  ],
})
export class ArticlesModule {}
