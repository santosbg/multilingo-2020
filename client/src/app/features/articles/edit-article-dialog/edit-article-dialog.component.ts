import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriesService } from '../../../core/services/categories.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { EditArticleDTO } from '../models/edit-article.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';

export interface DialogData {
  article: ShowArticleVersionDTO;
}

@Component({
  selector: 'app-edit-article-dialog',
  templateUrl: './edit-article-dialog.component.html',
  styleUrls: ['./edit-article-dialog.component.css']
})
export class EditArticleDialogComponent implements OnInit {
  public editArticleForm: FormGroup;
  public update: EventEmitter<EditArticleDTO> = new EventEmitter();
  public categories: CategoryDTO[];
  public photo: string;
  public selectedCategory: CategoryDTO;
  public ui: UIComponentDTO;

  constructor(
    public dialogRef: MatDialogRef<EditArticleDialogComponent>,
    private readonly categoriesService: CategoriesService,
    private readonly formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: { article: ShowArticleVersionDTO; ui: UIComponentDTO, categories: CategoryDTO[] }
  ) {}

  ngOnInit() {
    // and it thinks its ShowArticleInfoDTO , bruh its ShowArticleVersionDTO
    const dataForm: ShowArticleVersionDTO = this.data.article;
    this.ui = this.data.ui;
    this.photo = dataForm.image;


    this.selectedCategory = this.data.categories.find(
      e => e.id === dataForm.article.category.id
    );

    this.editArticleForm = this.formBuilder.group({
      title: [
        dataForm.title,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ]
      ],
      subtitle: [
        dataForm.subtitle,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ]
      ],
      content: [
        dataForm.content,
        [
          Validators.required,
          Validators.minLength(20),
          Validators.maxLength(3000),
        ]
      ]
    });
  }

  onAddPhoto(event) {
    this.photo = event;
  }

  onRemovePhoto() {
    this.photo = null;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.update.emit({
      ...this.editArticleForm.value,
      category: this.selectedCategory.id,
      image: this.photo
    });
  }
}
