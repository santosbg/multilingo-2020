import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ClickEvent } from 'angular-star-rating';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { ShowUserDTO } from '../../users/models/show-users.dto';
import { ArticleRatingsDTO } from '../models/article-ratings.dto';

@Component({
  selector: 'app-article-ratings',
  templateUrl: './article-ratings.component.html',
  styleUrls: ['./article-ratings.component.css']
})
export class ArticleRatingsComponent implements OnInit {
  @Input() ratings: ArticleRatingsDTO;
  @Input() ui: UIComponentDTO;
  @Input() public loggedUser: ShowUserDTO;
  public panelOpenState = false;


  @Output() rateText: EventEmitter<{
    rate: number;
    articleComponent: string;
    initRate: number | undefined;
  }> = new EventEmitter();
  public onClickResult;

  constructor() {}

  ngOnInit() {}

  onClick = ($event: ClickEvent) => {
    this.onClickResult = $event;

    return (articleComp: string, myRate: number | undefined) => {
      if ($event.rating === myRate || !this.loggedUser) {
        return;
      }
      this.rateText.emit({
        rate: $event.rating,
        articleComponent: articleComp,
        initRate: myRate
      });
    };
  }
}
