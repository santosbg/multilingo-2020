import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ArticlesService } from '../../../core/services/articles.service';
import { CategoriesService } from '../../../core/services/categories.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';
import { MESSAGES, PATHS } from '../../../common/constants';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-all-articles',
  templateUrl: './all-articles.component.html',
  styleUrls: ['./all-articles.component.css']
})
export class AllArticlesComponent implements OnInit, OnDestroy {
  language: string;
  articles: ShowArticleVersionDTO[];
  articlesToShow: ShowArticleVersionDTO[];
  ui: UIComponentDTO;
  categories: CategoryDTO[];

  private readonly subscriptions: Subscription[] = [];

  constructor(
    private readonly uiService: UIService,
    private readonly languageService: LanguagesService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly articlesService: ArticlesService,
    private readonly categoriesService: CategoriesService,
    private readonly notificationsService: NotificationService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      ({ data }): any => {
        this.articles = data.articles;
        this.articlesToShow = this.articles;
        this.ui = data.ui;
        this.language = data.language;
        this.categories = data.categories;
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );

    const languageSubscription: Subscription = this.languageService.displayLanguage$.subscribe(
      (currentLanguage: string) => {
        if (this.language !== currentLanguage) {
          this.language = currentLanguage;

          this.articlesService.getAllArticles(currentLanguage).subscribe(
            (articles: ShowArticleVersionDTO[]) => {
              this.articles = articles;
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );

          const uiSubscription = this.uiService.UI$.subscribe(
            (ui: UIComponentDTO) => {
              this.ui = ui;
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
          uiSubscription.unsubscribe();

          this.categoriesService.getAllCategories(currentLanguage).subscribe(
            (categories: CategoryDTO[]) => {
              this.categories = categories;
            },
            () => {
              this.notificationsService.error(MESSAGES.TRY_AGAIN);
            }
          );
        }
      },
      () => {
        this.notificationsService.error(MESSAGES.TRY_AGAIN);
      }
    );
    this.subscriptions.push(languageSubscription);
  }

  redirectToArticle(id: string) {
    this.router.navigateByUrl(`${PATHS.ARTICLES}${id}`);
  }

  filterArticlesByCategory(category: string) {
    if (category === 'all') {
      this.articlesToShow = this.articles;
    } else {
      this.articlesToShow = this.articles.filter((currentArticle: ShowArticleVersionDTO) => {
        return currentArticle.article.category.name === category;
      });
    }
  }

  ngOnDestroy() {
    this.subscriptions.map(currentSubscription => currentSubscription.unsubscribe());
  }
}
