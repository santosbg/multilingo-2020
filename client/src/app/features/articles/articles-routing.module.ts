import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoggedGuard } from '../../core/guards/logged.guard';
import { AllLanguagesResolver } from '../languages/resolvers/all-languages.resolver';
import { AllArticlesComponent } from './all-articles/all-articles.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { AllArticlesResolver } from './resolvers/all-articles.resolver';
import { AllCategoriesResolver } from './resolvers/all-categories.resolver';
import { ArticlesCategoryResolver } from './resolvers/articles-category.resolver';
import { SingleArticleResolver } from './resolvers/single-article.resolver';
import { SingleArticleComponent } from './single-article/single-article.component';

const routes = [
  {
    path: '',
    component: AllArticlesComponent,
    resolve: { data: AllArticlesResolver }
  },
  {
    path: 'create',
    component: CreateArticleComponent,
    canActivate: [LoggedGuard],
    resolve: {
      categories: AllCategoriesResolver,
      allLanguages: AllLanguagesResolver
    }
  },
  {
    path: 'categories',
    redirectTo: 'categories/news',
    pathMatch: 'full'
  },
  {
    path: ':id',
    component: SingleArticleComponent,
    resolve: { article: SingleArticleResolver }
  },
  {
    path: '**',
    redirectTo: '/not-found'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule {}
