import { ArticleUserDTO } from '../../articles/models/article-user.dto';
import { LanguageDTO } from '../../languages/models/language.dto';

export class TranslationDTO {
  id: string;
  originalText: string;
  originalLang: LanguageDTO;
  translatedText: string;
  targetLang: LanguageDTO;
  type: string;
  rating: number;
  translatedBy: ArticleUserDTO;
}
