import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationsService } from '../../../core/services/translation.service';

@Injectable()
export class AllTranslationsResolver implements Resolve<any> {
  constructor(private readonly translationsService: TranslationsService) {}

  resolve() {

    const data$ = zip(
      this.translationsService.getAllTranslations('article'),
      this.translationsService.getAllTranslations('ui-component')
    );

    return data$.pipe(
      map(
        ([articles, uiTransl]) => {

          return { articles, uiTransl };

      })
    );
  }
}
