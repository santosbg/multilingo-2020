import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslationDTO } from '../models/translation.dto';

@Component({
  selector: 'app-edit-translation-dialog',
  templateUrl: './edit-translation-dialog.component.html',
  styleUrls: ['./edit-translation-dialog.component.css']
})
export class EditTranslationDialogComponent implements OnInit {
  public editTranslationForm: FormGroup;
  public updateTranslation: EventEmitter<TranslationDTO> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<EditTranslationDialogComponent>,
    private readonly formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: TranslationDTO
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.updateTranslation.emit(this.editTranslationForm.value);
  }

  ngOnInit() {
    const dataForm: TranslationDTO = this.data;

    this.editTranslationForm = this.formBuilder.group({
      originalLang: [{ value: dataForm.originalLang.name, disabled: true }, []],
      originalText: [{ value: dataForm.originalText, disabled: true }, []],
      translatedText: [
        dataForm.translatedText,
        [Validators.required, Validators.minLength(3)]
      ],
      targetLang: [{ value: dataForm.targetLang.name, disabled: true }, []]
    });
  }
}
