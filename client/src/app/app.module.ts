import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from '@auth0/angular-jwt';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { RegisterComponent } from './components/register/register.component';
import { ServerErrorPageComponent } from './components/server-error-page/server-error-page.component';
import { CoreModule } from './core/core.module';
import { AdminGuard } from './core/guards/admin.guard';
import { EditorGuard } from './core/guards/editor.guard';
import { LoggedGuard } from './core/guards/logged.guard';
import { NotLoggedGuard } from './core/guards/not-logged.guard';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { SpinnerInterceptor } from './core/interceptors/spinner.interceptor';
import { AppInitService } from './core/services/app-init.service';
import { AuthService } from './core/services/auth.service';
import {
  ChangeLanguagesConfirmComponent
} from './features/admin-panel/components/admin-settings/change-languages-confirm/change-languages-confirm.component';
import { EditArticleDialogComponent } from './features/articles/edit-article-dialog/edit-article-dialog.component';
import {
  RevertArticleVersionComponent
} from './features/articles/revert-article-version/revert-article-version.component';
import { CommentsModule } from './features/comments/comments.module';
import {
  EditTranslationDialogComponent
} from './features/translations/edit-translation-dialog/edit-translation-dialog.component';
import {PaswordDialogComponent } from './features/users/components/password-dialog/password-dialog.component';
import {
  LanguageConfirmDialogComponent
} from './shared/components/language-confirm-dialog/language-confirm-dialog.component';
import { SharedModule } from './shared/shared.module';

export function initializeApp1(appInitService: AppInitService) {
  return (): Promise<any> => {
    return appInitService.Init();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LoginComponent,
    RegisterComponent,
    HomePageComponent,
    ServerErrorPageComponent,
    NotFoundPageComponent,
    EditTranslationDialogComponent,
    ChangeLanguagesConfirmComponent,
    LanguageConfirmDialogComponent,
    RevertArticleVersionComponent,
    EditArticleDialogComponent,
    PaswordDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
    CommentsModule,
    HttpClientModule,
    NgxSpinnerModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          const localSToken = localStorage.getItem('token');
          return localSToken ? localSToken : sessionStorage.getItem('token');
        },
        whitelistedDomains: ['localhost:3000']
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    AdminGuard,
    LoggedGuard,
    EditorGuard,
    NotLoggedGuard,
    LoggedGuard,
    AdminGuard,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp1,
      deps: [AppInitService, AuthService],
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    EditTranslationDialogComponent,
    LanguageConfirmDialogComponent,
    ChangeLanguagesConfirmComponent,
    RevertArticleVersionComponent,
    PaswordDialogComponent,
    EditArticleDialogComponent,
  ]
})
export class AppModule {}
