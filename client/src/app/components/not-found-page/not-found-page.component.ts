import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UIComponentDTO } from '../../models/ui-component.dto';
import { UIService } from '../../core/services/ui.service';

@Component({
  selector: 'app-not-found-page',
  templateUrl: './not-found-page.component.html',
  styleUrls: ['./not-found-page.component.css']
})
export class NotFoundPageComponent implements OnInit {

  public ui: UIComponentDTO;

  constructor(
    private readonly router: Router,
    private readonly uiService: UIService
  ) { }

  ngOnInit() {
    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
    );
  }

  public redirectToHomePage() {
    this.router.navigate(['/home']);
  }
}
