import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { LanguagesService } from '../../core/services/languages.service';
import { NotificationService } from '../../core/services/notification.service';
import { UIService } from '../../core/services/ui.service';
import { LanguageDTO } from '../../features/languages/models/language.dto';
import { UIComponentDTO } from '../../models/ui-component.dto';
import { MESSAGES, PATHS } from '../../common/constants';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  languages: LanguageDTO[];
  displayLanguage: string;

  @Input()
  ui: UIComponentDTO;

  constructor(
    private readonly router: Router,
    private readonly languagesService: LanguagesService,
    private readonly notificationService: NotificationService,
    private readonly uiService: UIService
  ) {}

  ngOnInit() {
    this.languagesService.getSuppLanguages().subscribe(
      (languagesData: LanguageDTO[]) => {
        this.languages = languagesData;
      },
      () => {
        this.notificationService.error(MESSAGES.TRY_AGAIN);
        this.router.navigate([PATHS.HOME]);
      }
    );

    this.languagesService.displayLanguage$.subscribe(
      (lang: string) => {
        this.displayLanguage = lang;
      },
      () => {
        this.notificationService.error(MESSAGES.TRY_AGAIN);
      }
    );

    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
      () => {
        this.notificationService.error(MESSAGES.TRY_AGAIN);
      }
    );
  }

  goToHomePage() {
    this.router.navigate([PATHS.HOME]);
  }
}
