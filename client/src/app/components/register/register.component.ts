import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../core/services/auth.service';
import { LanguagesService } from '../../core/services/languages.service';
import { NotificationService } from '../../core/services/notification.service';
import { UIService } from '../../core/services/ui.service';
import { LanguageDTO } from '../../features/languages/models/language.dto';
import { UIComponentDTO } from '../../models/ui-component.dto';
import { CONSTRAINTS, MESSAGES, PATHS } from '../../common/constants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  hidePassword = true;
  registerForm: FormGroup;

  @Input()
  displayLanguage: string;
  @Input()
  languages: LanguageDTO[];
  @Input()
  ui: UIComponentDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(CONSTRAINTS.MIN_USERNAME_LENGTH),
          Validators.maxLength(CONSTRAINTS.MAX_USERNAME_LENGTH)
        ])
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(CONSTRAINTS.MIN_PASSWORD_LENGTH),
          Validators.maxLength(CONSTRAINTS.MAX_PASSWORD_LENGTH)
        ])
      ],
      confirmPassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(CONSTRAINTS.MIN_PASSWORD_LENGTH),
          Validators.maxLength(CONSTRAINTS.MAX_PASSWORD_LENGTH)
        ])
      ],
      prefLang: ['', Validators.required],
    });
  }

  register() {
    const registerFormValue = { ...this.registerForm.value };
    delete registerFormValue.confirmPassword;

    this.authService.register(registerFormValue).subscribe(
      () => {
        this.notificationService.success(MESSAGES.REGISTRATION_SUCCESSFUL);
        /**
         * After the registration the user
         * is logged in automatically
         */
        this.authService.login(this.registerForm.value).subscribe(
          ({ token }) => {
            const authUser = this.authService.getDecodedAccessToken(token);
            if (authUser.prefLang !== this.displayLanguage) {
              this.uiService.fireUISubjectInLang(authUser.prefLang).subscribe(
                () => {
                  this.languagesService.changeLanguage(authUser.prefLang);
                },
                () => this.notificationService.error(MESSAGES.TRY_AGAIN)
              );
            }
            this.notificationService.success(MESSAGES.LOGIN_SUCCESSFUL);
            this.router.navigate([PATHS.HOME]);
          },
          () => this.notificationService.error(MESSAGES.INVALID_CREDENTIALS)
        );
      },
      () => this.notificationService.error(MESSAGES.TRY_AGAIN)
    );
  }
}
