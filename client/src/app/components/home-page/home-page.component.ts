import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticlesService } from '../../core/services/articles.service';
import { AuthService } from '../../core/services/auth.service';
import { LanguagesService } from '../../core/services/languages.service';
import { UIService } from '../../core/services/ui.service';
import { ShowArticleVersionDTO } from '../../features/articles/models/show-article-version.dto';
import { LoggedUserDTO } from '../../features/users/models/logged-user.dto';
import { UIComponentDTO } from '../../models/ui-component.dto';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public latestArticles = [];
  public language: string;
  public loggedUser: LoggedUserDTO;

  public ui: UIComponentDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly uiService: UIService,
    private readonly articlesService: ArticlesService,
    private readonly languagesService: LanguagesService,
  ) { }

  ngOnInit() {
    this.languagesService.displayLanguage$.subscribe(
      (lang: string) => {
        this.language = lang;

        this.articlesService.getLatestArticlesInLang(this.language).subscribe(
          (articles: ShowArticleVersionDTO[]) => {
            this.latestArticles = articles;
          }
        );
      }
    );

    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
    );

    this.authService.loggedUser$.subscribe(
      (loggedUser) => {
        this.loggedUser = loggedUser;
      }
    );

  }

  public startWriting() {
    if (this.loggedUser === null) {
      this.router.navigate(['/landing']);
    } else {
      this.router.navigate(['/articles/create']);
    }
  }

  public navigateToArticle(articleId: string) {
    this.router.navigate([`/articles/${articleId}`]);
  }
}
