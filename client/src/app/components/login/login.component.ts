import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../core/services/auth.service';
import { LanguagesService } from '../../core/services/languages.service';
import { NotificationService } from '../../core/services/notification.service';
import { UIService } from '../../core/services/ui.service';
import { UIComponentDTO } from '../../models/ui-component.dto';
import { CONSTRAINTS, MESSAGES, PATHS } from '../../common/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hidePassword = true;
  loginForm: FormGroup;

  @Input()
  displayLanguage: string;
  @Input()
  ui: UIComponentDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(CONSTRAINTS.MIN_USERNAME_LENGTH),
          Validators.maxLength(CONSTRAINTS.MAX_USERNAME_LENGTH)
        ])
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(CONSTRAINTS.MIN_PASSWORD_LENGTH),
          Validators.maxLength(CONSTRAINTS.MAX_PASSWORD_LENGTH)
        ]),
      ],
      keepMeLoggedIn: [false]
    });
  }

  login() {
    this.authService.login(this.loginForm.value).subscribe(
      ({ token }) => {
        const user = this.authService.getDecodedAccessToken(token);
        if (user.prefLang !== this.displayLanguage) {
          this.uiService.fireUISubjectInLang(user.prefLang).subscribe(
            () => {
              this.languagesService.changeLanguage(user.prefLang);
            },
            () => {
              this.notificationService.error(MESSAGES.TRY_AGAIN);
            }
          );
        }
        this.notificationService.success(MESSAGES.LOGIN_SUCCESSFUL);
        this.router.navigate([PATHS.HOME]);
      },
      () => this.notificationService.error(MESSAGES.TRY_AGAIN)
    );
  }
}
