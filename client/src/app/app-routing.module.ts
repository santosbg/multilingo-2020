import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { ServerErrorPageComponent } from './components/server-error-page/server-error-page.component';
import { NotLoggedGuard } from './core/guards/not-logged.guard';
import { AllCommentsComponent } from './features/comments/all-comments/comments.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'articles',
    loadChildren: () =>
      import('./features/articles/articles.module').then(m => m.ArticlesModule),
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'admin-panel',
    loadChildren: () =>
      import('./features/admin-panel/admin-panel.module').then(
        m => m.AdminPanelModule
      )
  },
  {
    path: 'users',
    loadChildren: () =>
      import('./features/users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'translations',
    loadChildren: () =>
      import('./features/translations/translations.module').then(
        m => m.TranslationsModule
      )
  },
  {
    path: 'landing',
    component: LandingPageComponent,
    canActivate: [NotLoggedGuard]
    // resolve: { ui: UIResolver }
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'comments',
    component: AllCommentsComponent
  },
  {
    path: 'not-found',
    component: NotFoundPageComponent
  },
  {
    path: 'server-error',
    component: ServerErrorPageComponent
  },
  {
    path: '**',
    redirectTo: '/not-found'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
