import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {

  @Output() search = new EventEmitter<string>();

  @Input()
  public ui;

  public triggerSearch(key: string) {
    this.search.emit(key);
  }

}
