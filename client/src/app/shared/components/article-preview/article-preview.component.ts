import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { ShowArticleVersionDTO } from '../../../features/articles/models/show-article-version.dto';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { PATHS } from '../../../common/constants';

@Component({
  selector: 'app-article-preview',
  templateUrl: './article-preview.component.html',
  styleUrls: ['./article-preview.component.css']
})
export class ArticlePreviewComponent {
  @Input()
  ui: UIComponentDTO;
  @Input()
  article: ShowArticleVersionDTO;
  @Output()
  goToArticle = new EventEmitter<string>();

  constructor(
    private readonly router: Router
  ) {}

  seeArticle(id: string) {
    this.goToArticle.emit(id);
  }

  viewUser(id: string) {
    this.router.navigateByUrl(`${PATHS.USERS}${id}`);
  }
}
