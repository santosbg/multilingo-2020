import { Component, OnInit, Input } from '@angular/core';

import { LoggedUserDTO } from '../../../features/users/models/logged-user.dto';
import { LanguageDTO } from '../../../features/languages/models/language.dto';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { NotificationService } from '../../../core/services/notification.service';
import { MESSAGES } from '../../../common/constants';

@Component({
  selector: 'app-select-language',
  templateUrl: './select-language.component.html',
  styleUrls: ['./select-language.component.css']
})
export class SelectLanguageComponent {
  @Input()
  loggedUser: LoggedUserDTO;
  @Input()
  supportedLanguages: LanguageDTO[] = [];
  @Input()
  selectedLanguage: string;

  constructor(
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly notificationService: NotificationService
  ) {}

  onChange() {
    this.uiService.fireUISubjectInLang(this.selectedLanguage).subscribe(
      () => {
        this.languagesService.changeLanguage(this.selectedLanguage);
      },
      () => this.notificationService.error(MESSAGES.TRY_AGAIN)
    );

    this.languagesService.displayLanguage$.subscribe(
      (languageData: string) => {
        this.selectedLanguage = languageData;
      },
      () => this.notificationService.error(MESSAGES.TRY_AGAIN)
    );
  }
}
