import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../core/services/auth.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { NotificationService } from '../../../core/services/notification.service';
import { UIService } from '../../../core/services/ui.service';
import { LanguageDTO } from '../../../features/languages/models/language.dto';
import { LoggedUserDTO } from '../../../features/users/models/logged-user.dto';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { MESSAGES, PATHS, LANGUAGES_INDEX } from '../../../common/constants';

@Component({
  selector: 'app-navigation',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavigationComponent {
  @Input()
  ui: UIComponentDTO;
  @Input()
  loggedUser: LoggedUserDTO;
  @Input()
  supportedLanguages: LanguageDTO[] = [];
  @Input()
  selectedLanguage: string;

  detectedLanguage: string;

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly notificationService: NotificationService
  ) {}

  toUsers() {
    this.router.navigate([PATHS.USERS]);
  }

  toUserProfile() {
    this.router.navigate([PATHS.USERS, this.loggedUser.id]);
  }

  toAdminPanel() {
    this.router.navigate([PATHS.ADMIN_PANEL]);
  }

  toSignIn() {
    this.router.navigate([PATHS.LANDING]);
  }

  toLogout() {
    try {
      this.authService.logout();
      this.notificationService.success(MESSAGES.SUCCESSFUL_LOGOUT);
      const languages = navigator.languages;
      let isLogoutDone = false;

      for (let currentLanguage of languages) {
        if (currentLanguage.match('-')) {
          currentLanguage = currentLanguage.split('-')[0];
        }
        const foundLanguage = this.supportedLanguages.find(currentSuppeortedLanguage => {
          return currentSuppeortedLanguage.index === currentLanguage;
        });

        if (foundLanguage) {
          this.languagesService.changeLanguage(foundLanguage.index);
          this.uiService.fireUISubjectInLang(foundLanguage.index).subscribe(
            () => {
              this.notificationService.success(`${MESSAGES.CHANGED_LANGUAGE} ${foundLanguage.name}`);
            },
            () => this.notificationService.error(MESSAGES.TRY_AGAIN)
          );
          isLogoutDone = true;
          break;
        }
      }

      if (!isLogoutDone) {
        this.uiService.fireUISubjectInLang(LANGUAGES_INDEX.EN).subscribe(
          () => {
            this.languagesService.changeLanguage(LANGUAGES_INDEX.EN);
          },
          () => this.notificationService.error(MESSAGES.TRY_AGAIN)
        );
      }

      this.router.navigate([PATHS.HOME]);
    } catch (error) {
      this.router.navigate([PATHS.HOME]);
      this.notificationService.error(MESSAGES.TRY_AGAIN);
    }
  }

  toArticles() {
    this.router.navigate([PATHS.ARTICLES]);
  }

  toCreateArticles() {
    this.router.navigate([PATHS.CREATE_ARTICLE]);
  }

  toTranslations() {
    this.router.navigate([PATHS.TRANSLATION]);
  }

  toCreateTranslation() {
    this.router.navigate([PATHS.CREATE_TRANSLATION]);
  }
}
