import { Component, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

import { CategoryDTO } from '../../../features/admin-panel/components/admin-settings/categories/models/category.dto';

@Component({
  selector: 'app-categories-navigation',
  templateUrl: './categories-navigation.component.html',
  styleUrls: ['./categories-navigation.component.css']
})
export class CategoriesNavigationComponent {
  language: string;
  @Input()
  categories: CategoryDTO[];
  @Output()
  filterByCategoryEvent: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('categoryLabel', { static: true })
  categoryLabelElement: ElementRef;

  selectCategory(categoryName: string) {
    this.filterByCategoryEvent.emit(categoryName);
    this.categoryLabelElement.nativeElement.innerHTML = categoryName;
  }
}
