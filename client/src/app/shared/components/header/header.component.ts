import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { NotificationService } from '../../../core/services/notification.service';
import { MESSAGES, PATHS } from '../../../common/constants';
import { AuthService } from '../../../core/services/auth.service';
import { LoggedUserDTO } from '../../../features/users/models/logged-user.dto';
import { LanguageDTO } from '../../../features/languages/models/language.dto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  ui: UIComponentDTO;
  loggedUser: LoggedUserDTO;
  supportedLanguages: LanguageDTO[] = [];
  selectedLanguage: string;

  constructor(
    private readonly router: Router,
    private readonly uiService: UIService,
    private readonly languageService: LanguagesService,
    private readonly notificationsService: NotificationService,
    private readonly authService: AuthService
  ) { }

  ngOnInit() {
    this.languageService.displayLanguage$.subscribe(
      () => {
        this.uiService.UI$.subscribe(
          (ui) => {
            this.ui = ui;
          },
          () => this.notificationsService.error(MESSAGES.TRY_AGAIN)
        );
      },
      () => this.notificationsService.error(MESSAGES.TRY_AGAIN)
    );

    this.authService.loggedUser$.subscribe(
      loggedUser => {
        this.loggedUser = loggedUser;
      },
      () => this.notificationsService.error(MESSAGES.TRY_AGAIN)
    );

    this.languageService.getSuppLanguages().subscribe(
      (supportedLanguagesData: LanguageDTO[]) => {
        this.supportedLanguages = supportedLanguagesData;
      },
      () => this.notificationsService.error(MESSAGES.TRY_AGAIN)
    );

    this.languageService.displayLanguage$.subscribe(
      (languageData: string) => {
        this.selectedLanguage = languageData;
      },
      () => this.notificationsService.error(MESSAGES.TRY_AGAIN)
    );
  }

  goToHome() {
    this.router.navigate([PATHS.HOME]);
  }
}
