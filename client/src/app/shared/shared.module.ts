import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { StarRatingModule } from 'angular-star-rating';
import { ImageCropperModule } from './../features/image-croper/image-croper.module';
import { ArticlePreviewComponent } from './components/article-preview/article-preview.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/nav-bar/nav-bar.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { UploadImageComponent } from '../features/articles/upload-image/upload-image.component';
import { SelectLanguageComponent } from './components/select-language/select-language.component';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    SearchBarComponent,
    UploadImageComponent,
    ArticlePreviewComponent,
    SelectLanguageComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    StarRatingModule.forRoot(),
    AngularDualListBoxModule,
    MatTabsModule,
    MatIconModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCheckboxModule,
    HttpClientModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatExpansionModule,
    MatSelectModule,
    MatCardModule,
    MatExpansionModule,
    HttpClientModule,
    MatDividerModule,
    MatChipsModule,
    MatAutocompleteModule,
    ImageCropperModule
  ],
  exports: [
    ArticlePreviewComponent,
    UploadImageComponent,
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    CommonModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    StarRatingModule,
    AngularDualListBoxModule,
    MatToolbarModule,
    MatTabsModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCheckboxModule,
    HttpClientModule,
    MatMenuModule,
    SearchBarComponent,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule,
    MatCardModule,
    MatDividerModule,
    HttpClientModule,
    MatAutocompleteModule
  ]
})
export class SharedModule {}
