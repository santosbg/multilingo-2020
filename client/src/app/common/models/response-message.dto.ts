export class ResponseMessageDTO {
    public message: string;
    public code?: number;
}
